// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package conf

import (
	"io/ioutil"
	"os"

	"bitbucket.org/abex/yumeko/util"
	"gopkg.in/ini.v1"
)

//loadFile opens reads and writes the config file specified by name
func loadFile(name string) error {
	fi, err := os.OpenFile(name, os.O_CREATE|os.O_RDWR, 0660)
	if err != nil {
		return err
	}
	defer fi.Close()
	err = util.Lock(fi)
	if err != nil {
		return err
	}
	orig, err := ioutil.ReadAll(fi)
	if err != nil {
		return err
	}
	needrw := false
	cini, err := ini.Load(orig)
	if err != nil {
		return err
	}
	for _, kvpair := range config {
		sec := cini.Section(kvpair.prefix) // create if not exist
		var val string
		if sec.HasKey(kvpair.key) {
			val = sec.Key(kvpair.key).Value()
		} else {
			needrw = true
			val = kvpair.defVal
			key, err := sec.NewKey(kvpair.key, val)
			if err != nil {
				return err
			}
			key.Comment = kvpair.comment
		}
		err := setSend(kvpair.val, val)
		if err != nil {
			return err
		}
	}
	if needrw {
		_, err := fi.Seek(0, os.SEEK_SET)
		if err != nil {
			return err
		}
		n, err := cini.WriteToIndent(fi, "")
		err2 := fi.Truncate(n)
		if err != nil {
			return err
		}
		if err2 != nil {
			return err2
		}
	}
	return nil
}
