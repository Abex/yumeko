// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package conf

import (
	"os"
	"path/filepath"
	"reflect"
	"time"

	"bitbucket.org/abex/yumeko/glug"
	"gopkg.in/fsnotify/fsnotify.v1"
	ini "gopkg.in/ini.v1"
)

var log = glug.NewLogger("config")

var basePath string

// Load should be called with a path to the config directory
// It should contain a config.ini and will be created as necesary
// All Config calls should be done. Path and OpenFile can be called after this
func Load(path string) {
	basePath = path
	err := OpenFile("config.ini", func(_ struct{}, err error) {
		if err != nil {
			panic(err)
		}
		err = loadFile(Path("config.ini"))
		if err != nil {
			panic(err)
		}
	})
	if err != nil {
		panic(err)
	}
}

func Path(end string) string {
	return filepath.Join(basePath, end)
}

// OpenFile will take a name relative to the config Root as passed to Load
// and update var accordingly. Val can be:
// - func (T, error)
// - chan T
// - *T
// T may be:
// - struct{}
// - *os.File
// - string
// - []byte
// - interface{} which will be JSON decoded
func OpenFile(name string, val interface{}) error {
	return OpenFileDefault(name, val, "")
}

// OpenFileDefault is like OpenFile but will fill the file with the contents of default if it doesn't exists or is empty
func OpenFileDefault(name string, val interface{}, defaul string) error {
	name = Path(name)
	stat, err := os.Stat(name)
	if os.IsNotExist(err) || stat.Size() == 0 {
		err = nil
		fi, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			return err
		}
		_, err = fi.WriteString(defaul)
		fi.Close()
		if err != nil {
			return err
		}
	}
	if err != nil {
		return err
	}
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	log.Trace("Watching", filepath.Dir(name))
	watcher.Add(filepath.Dir(name))
	an, err := filepath.Abs(name)
	if err != nil {
		panic(err)
	}
	vo := reflect.ValueOf(val)
	go func() {
		defer watcher.Close()
		notime := make(<-chan time.Time)
		var nt *time.Timer
		ctime := notime
		for {
			select {
			case event := <-watcher.Events:
				log.Tracef("Event %v: %v", name, event)
				if event.Op&(fsnotify.Write|fsnotify.Rename) != 0 {
					ean, err := filepath.Abs(event.Name)
					if err != nil {
						panic(err)
					}
					if an == ean {
						if nt != nil {
							if !nt.Stop() {
								<-nt.C
							}
						}
						nt = time.NewTimer(time.Millisecond * 250)
						ctime = nt.C
					}
				}
			case err := <-watcher.Errors:
				log.Weirdf("Error while watching %v: %v", name, err)
			case <-ctime:
				if nt != nil {
					nt = nil
				}
				ctime = notime
				err := fileUpdate(name, vo)
				if err != nil {
					log.Errorf("Error reloading %v: %v", name, err)
				}
				tm := time.NewTimer(time.Millisecond * 50).C
				for {
					select {
					case <-tm:
						break
					case <-watcher.Events:
					}
				}
			}
		}
	}()
	return fileUpdate(name, vo)

}

// Config registers a configuration value. This must be done before calling Load()
// Val can be:
// - func (T, error)
// - chan T
// - *T
// T may be anything supported by https://github.com/go-ini/ini
// Prefix should be a non-empty string for the table name
// defStr should contain valid TOML that is used as the default
// Such as `# Enable my key
// enable = true
// `
// Note their is no table, and a trailing newline
func Config(val interface{}, prefix string, defStr string) {
	cini, err := ini.Load(([]byte)(defStr))
	if err != nil {
		panic(err)
	}
	sects := cini.Sections()
	if len(sects) <= 0 {
		panic("default string is missing keys")
	}
	keys := sects[len(sects)-1].Keys()
	if len(keys) <= 0 {
		panic("default string is missing keys")
	}
	key := keys[len(keys)-1]
	pkey := key.Name()
	log.Trace("Adding config", glug.KV{
		"prefix": prefix,
		"pkey":   pkey,
	})
	pair := kvpair{
		val:     reflect.ValueOf(val),
		prefix:  prefix,
		key:     pkey,
		comment: key.Comment,
		defVal:  key.Value(),
	}
	config = append(config, pair)
}

var config []kvpair

type kvpair struct {
	val     reflect.Value
	prefix  string
	key     string
	comment string
	defVal  string
}
