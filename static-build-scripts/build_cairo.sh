[ -e "$INSTALL_DIR/lib/libcairo.a" ]&&exit 0
get_and_extract cairo http://cairographics.org/snapshots/cairo-1.15.8.tar.xz -J
pushd cairo/*
LDFLAGS="$LDFLAGS -Wl,--start-group -lfreetype -lharfbuzz -Wl,--end-group" configure_args ./configure --disable-xlib --disable-xcb --disable-qt --disable-quartz\
	--disable-win32 --disable-skia --disable-os2 --disable-beos --disable-drm --disable-gallium --disable-gl --disable-glesv2\
	--disable-cogl --disable-directfb --disable-vg --disable-egl --disable-glx --disable-wgl --disable-fc\
	--disable-ps --disable-pdf --disable-svg --disable-xml --disable-gobject --disable-trace --disable-interpreter --enable-ft
make_args make V=1 -j1
make_args make install
popd
rm -rf cairo