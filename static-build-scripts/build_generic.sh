[ -e "$INSTALL_DIR/??" ]&&exit 0
[ -e ?? ]||git clone --branch ?? --depth 1 ??
pushd ??
configure_args ./autogen.sh
configure_args ./configure
make_args make
make_args make install
popd
rm -rf ??