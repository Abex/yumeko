[ -e "$INSTALL_DIR/bin/$TARGET-gcc" ]&&exit 0
if ! [ -e musl-cross-make ]; then
	git clone --depth=1 git@github.com:richfelker/musl-cross-make.git
fi
pushd musl-cross-make/
make_args make "TARGET=$TARGET" "OUTPUT=$INSTALL_DIR"
make_args make install "TARGET=$TARGET" "OUTPUT=$INSTALL_DIR"
popd

rm -rf musl-cross-make