[ -e "$INSTALL_DIR/lib/libvpx.a" ]&&exit 0
[ -e libvpx ]||git clone --depth 1 https://chromium.googlesource.com/webm/libvpx.git
pushd libvpx
CROSS="$TARGET-" configure_args_nocross ./configure --disable-examples --disable-unit-tests\
 --enable-vp9-highbitdepth --as=yasm "--target=generic-gnu" --enable-static --disable-shared
make_args make
make_args make install
popd
rm -rf libvpx