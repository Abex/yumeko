[ -e "$INSTALL_DIR/lib/libz.a" ]&&exit 0
get_and_extract zlib https://zlib.net/zlib-1.2.11.tar.gz
pushd zlib/*
cross_in_cc configure_args_nocross ./configure --static
cross_in_cc make_args make
cross_in_cc make_args make install
popd
rm -rf zlib