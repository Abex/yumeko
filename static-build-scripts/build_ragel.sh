[ -e "$INSTALL_DIR/bin/ragel" ]&&exit 0
get_and_extract ragel http://www.colm.net/files/ragel/ragel-6.10.tar.gz
pushd ragel/*
configure_args ./configure
make_args make
make_args make install "PREFIX=$INSTALL_DIR"
popd
rm -rf ragel