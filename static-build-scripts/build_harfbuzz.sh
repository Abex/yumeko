[ -e "$INSTALL_DIR/lib/libharfbuzz.a" ]&&exit 0
get_and_extract harfbuzz https://www.freedesktop.org/software/harfbuzz/release/harfbuzz-2.4.0.tar.bz2 -j
pushd harfbuzz/*
configure_args ./configure --without-gobject --without-glib --without-fontconfig --without-icu --without-graphite2\
 --with-freetype --without-uniscribe --without-directwrite --without-cairo
make_args make VERBOSE=1
make_args make install "PREFIX=$INSTALL_DIR"
popd
rm -rf harfbuzz