[ -e "$INSTALL_DIR/lib/libgme.a" ]&&exit 0
[ -e "libgme" ]||git clone --depth 1 https://Abex@bitbucket.org/mpyne/game-music-emu.git libgme
pushd libgme
cmake_args cmake -DENABLE_UBSAN=OFF -DBUILD_SHARED_LIBS=OFF
make_args make
make_args make install
popd
rm -rf libgme