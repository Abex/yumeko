[ -e "$INSTALL_DIR/lib/libx264.a" ]&&exit 0
[ -e x264 ]||git clone --depth 1 http://git.videolan.org/git/x264
pushd x264
configure_args ./configure  "--cross-prefix=$TARGET-"
make_args make
make_args make install
popd
rm -rf x264