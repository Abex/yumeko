[ -e "$INSTALL_DIR/lib/libogg.a" ]&&exit 0
[ -e ogg ]||git clone --depth 1 https://github.com/xiph/ogg.git
pushd ogg
configure_args ./autogen.sh
configure_args ./configure
make_args make
make_args make install
popd
rm -rf ogg