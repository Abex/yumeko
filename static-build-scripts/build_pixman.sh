[ -e "$INSTALL_DIR/lib/libpixman-1.a" ]&&exit 0
get_and_extract pixman  https://www.cairographics.org/releases/pixman-0.38.4.tar.gz
pushd pixman/*
configure_args ./configure --disable-openmp
make_args make
make_args make install
popd
rm -rf pixman