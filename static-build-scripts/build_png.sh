[ -e "$INSTALL_DIR/lib/libpng.a" ]&&exit 0
get_and_extract png https://download.sourceforge.net/libpng/libpng-1.6.37.tar.gz
pushd png/*
configure_args ./configure
make_args make
make_args make install
popd
rm -rf png