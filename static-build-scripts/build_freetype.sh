[ -e "$INSTALL_DIR/lib/libfreetype.a" ]&&[ -e "$INSTALL_DIR/lib/freetype_has_harfbuzz" ]&&exit 0
get_and_extract freetype https://download.savannah.gnu.org/releases/freetype/freetype-2.10.1.tar.gz
pushd freetype/*
if [ -e "$INSTALL_DIR/lib/libharfbuzz.a" ]; then
	EXTRA=--with-harfbuzz=yes
else
	EXTRA=--with-harfbuzz=no
fi
configure_args ./configure --with-zlib=yes --with-bzip2=yes --with-png=yes $EXTRA
make_args make
make_args make install
make_args make distclean
popd
if [ -e "$INSTALL_DIR/lib/libharfbuzz.a" ]; then
	pushd $INSTALL_DIR/lib/
	#libtool is too retarded to link to this clusterfuck, so just link them together here and ignore harfbuzz as it's own entity
	mv libfreetype.a libfreetype_slim.a
	"$TARGET-ar" -M <<EOF
CREATE libfreetype.a
ADDLIB libfreetype_slim.a
ADDLIB libharfbuzz.a
SAVE
END
EOF
	popd
	rm -rf freetype
	touch "$INSTALL_DIR/lib/freetype_has_harfbuzz"
fi