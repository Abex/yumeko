[ -e "$INSTALL_DIR/lib/libass.a" ]&&exit 0
[ -e libass ]||git clone --branch 0.14.0 --depth 1 https://github.com/libass/libass.git
pushd libass
configure_args ./autogen.sh
configure_args ./configure --disable-coretext --disable-directwrite --disable-fontconfig --disable-require-system-font-provider
make_args make
make_args make install
popd
rm -rf libass