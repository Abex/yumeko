[ -e "$INSTALL_DIR/bin/nasm" ]&&exit 0
[ -e nasm ]||git clone --branch nasm-2.14.02 --depth 1 http://repo.or.cz/nasm.git
pushd nasm
./autogen.sh
configure_args ./configure
make_args make
#make fails on (non disable-able) documentation
make_args make install || nasm -v
popd
rm -rf nasm