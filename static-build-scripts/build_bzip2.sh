[ -e "$INSTALL_DIR/lib/libbz2.a" ]&&exit 0
get_and_extract bzip2 https://downloads.sourceforge.net/project/bzip2/bzip2-1.0.6.tar.gz
pushd bzip2/*
cross_in_cc make_args make
cross_in_cc make_args make install "PREFIX=$INSTALL_DIR"
popd
rm -rf bzip2