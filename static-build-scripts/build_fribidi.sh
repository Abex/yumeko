[ -e "$INSTALL_DIR/lib/libfribidi.a" ]&&exit 0
[ -e fribidi ]||git clone --depth 1 https://github.com/fribidi/fribidi.git
pushd fribidi
configure_args ./autogen.sh --disable-docs
#build is broken when parallel (a831b5a) (11/30/17)
make_args make -j1
make_args make install
popd
rm -rf fribidi