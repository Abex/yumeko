[ -e "$INSTALL_DIR/lib/libopus.a" ]&&exit 0
[ -e opus ] || git clone --branch v1.3.1 --depth 1 https://github.com/xiph/opus.git
pushd opus
./autogen.sh
configure_args ./configure --disable-doc --disable-extra-programs
make_args make 
make_args make install
popd
rm -rf opus