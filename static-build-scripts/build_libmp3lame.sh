[ -e "$INSTALL_DIR/lib/libmp3lame.a" ]&&exit 0
get_and_extract lame http://downloads.sourceforge.net/project/lame/lame/3.100/lame-3.100.tar.gz
pushd lame/*
configure_args ./configure --enable-nasm
make_args make
make_args make install
popd
rm -rf lame