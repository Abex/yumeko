[ -e "$INSTALL_DIR/lib/libvorbis.a" ]&&exit 0
[ -e vorbis ]||git clone --branch v1.3.6 --depth 1 https://github.com/xiph/vorbis.git
pushd vorbis
configure_args ./autogen.sh
configure_args ./configure --with-ogg="$INSTALL_DIR"
make_args make
make_args make install
popd
rm -rf vorbis