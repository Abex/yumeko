[ -e "$INSTALL_DIR/lib/libx265.a" ]&&exit 0
[ -e x265 ]||hg clone https://bitbucket.org/multicoreware/x265
pushd x265/build/linux
cmake_args cmake -DENABLE_LIBNUMA:bool=off ../../source
make_args make
make_args make install
popd
rm -rf x265