[ -e "$INSTALL_DIR/bin/yasm" ]&&exit 0
[ -e yasm ]||git clone --branch v1.3.0 --depth 1 https://github.com/yasm/yasm.git
pushd yasm
configure_args ./autogen.sh
make_args make
make_args make install
popd
rm -rf yasm