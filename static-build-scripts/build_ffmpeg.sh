[ -e "$INSTALL_DIR/bin/ffmpeg" ]&&exit 0
if ! [ -e FFmpeg ]; then
  git clone --branch release/4.2 --depth 1 https://github.com/FFmpeg/FFmpeg.git
fi
pushd FFmpeg
configure_args_nocross ./configure \
  "--arch=$TARGET_ARCH" \
  "--target-os=$TARGET_OS" \
  "--cross-prefix=$TARGET-" \
  "--pkg-config=$(which pkg-config)"\
  --pkg-config-flags=--static \
  --extra-cflags="-I$INSTALL_DIR/include" \
  --extra-ldflags="-L$INSTALL_DIR/lib" \
  --extra-libs="-lpthread -lm" \
  --enable-static \
  --disable-shared \
  --disable-ffplay \
  --enable-pthreads \
  --disable-devices \
  --enable-gpl \
  --enable-libfdk_aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libx265 \
  --enable-nonfree \
  --enable-libass \
  --enable-libgme
make_args make
make_args make install
popd
rm -rf FFmpeg