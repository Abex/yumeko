[ -e "$INSTALL_DIR/lib/libfdk-aac.a" ]&&exit 0
[ -e fdk-aac ]||git clone --depth 1 https://github.com/mstorsjo/fdk-aac
pushd fdk-aac
autoreconf -fiv
configure_args ./configure
make_args make
make_args make install
popd
rm -rf fdk-aac