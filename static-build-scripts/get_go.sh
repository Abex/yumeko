#!/bin/bash
set -e -x
go version&&return 0
export GOROOT="$INSTALL_DIR/go"
export PATH="$GOROOT/bin:$PATH"
go version&&return 0
[ -e go.tar.gz ] || wget -Ogo.tar.gz https://dl.google.com/go/go1.12.9.linux-amd64.tar.gz
tar -xf go.tar.gz -C deps/
rm go.tar.gz