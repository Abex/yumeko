[ -e "$INSTALL_DIR/lib/libjpeg.a" ]&&exit 0
[ -e libjpeg-turbo ]||git clone --branch 2.0.2 --depth 1 https://github.com/libjpeg-turbo/libjpeg-turbo.git
pushd libjpeg-turbo
cmake_args cmake .
make_args make
make_args make install
popd
rm -rf libjpeg-turbo