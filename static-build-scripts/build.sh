#!/bin/bash
set -e -x
export ROOT="$(pwd)"
export YUMEKO_SRC_DIR="$ROOT/.."
export INSTALL_DIR=$ROOT/deps

if [ "$1" == "clean" ]; then
	rm -rf $(cat .gitignore)
	exit 0
fi

if [ ! -e "$INSTALL_DIR" ] ; then 
	mkdir $INSTALL_DIR
	mkdir $INSTALL_DIR/lib
	ln -s $INSTALL_DIR/lib $INSTALL_DIR/lib64
fi
export PATH="$INSTALL_DIR/bin:$PATH"
export PKG_CONFIG_PATH="$INSTALL_DIR/lib/pkgconfig"

export BUILD="$(gcc -dumpmachine)"
export TARGET_ARCH=x86_64
export TARGET_OS=linux
export TARGET="$TARGET_ARCH-$TARGET_OS-musl"

function build() {
	bash -e -x "$ROOT/build_$1.sh"
}
export -f build

function make_args() {
	SCRIPT="$1"
	shift
	"$SCRIPT" "-j12" "$@"
}
export -f make_args

function configure_args() {
	"$@" "--prefix=$INSTALL_DIR"
}
export -f configure_args

function get_and_extract() {
	if ! [ -e "$1" ]; then
		mkdir "$1"
		if ! curl -L "$2" | tar -x "${3:--z}" -C "$1"; then
			rm -rf "$1"
			false
		fi
	fi
}
export -f get_and_extract

build musl
build yasm
build nasm
build ragel

hash -r

export CPPFLAGS="-I$INSTALL_DIR/include/"
export CFLAGS="$CPPFLAGS -static --static -no-pie"
export LDFLAGS="-L$INSTALL_DIR/lib/ -static --static"

function configure_args_nocross() {
	"$@" "--prefix=$INSTALL_DIR"
}
export -f configure_args_nocross

function configure_args() {
	configure_args_nocross "$@" "--host=$TARGET" "--build=$BUILD" --enable-static --disable-shared "CFLAGS=$CFLAGS" "LDFLAGS=$LDFLAGS"
}
export -f configure_args

function cross_in_cc() {
	CC="$TARGET-gcc" CXX="$TARGET-g++" LD="$TARGET-ld" "$@"
}
export -f cross_in_cc

function cmake_args() {
	CMAKE="$1"
	shift
	$CMAKE "-GUnix Makefiles" "-DCMAKE_INSTALL_PREFIX=$INSTALL_DIR" -DCMAKE_SYSTEM_NAME=Linux "-DCMAKE_C_COMPILER=$TARGET-gcc"\
	"-DCMAKE_FIND_ROOT_PATH=$INSTALL_DIR" -DCMAKE_BUILD_TYPE=Release "-DCMAKE_SYSTEM_PROCESSOR=$TARGET_ARCH" -DENABLE_SHARED=false "$@"
}
export -f cmake_args

build zlib
build bzip2

build opus
build libjpeg_turbo

build png
build pixman
build fribidi
build freetype
build harfbuzz
build freetype
build cairo
build libass
build libogg
build vorbis
build x264
build x265
build libfdk_aac
build libmp3lame
build libvpx
build libgme
build ffmpeg

# Pure yumeko deps
source ./get_go.sh

# Build yumeko
rm -rf bin||true
mkdir bin
gobuild() {
	CGO_ENABLED="${CGO_ENABLED:-0}" go build -v -x -o "bin/$(basename $1)" "$YUMEKO_SRC_DIR/${2-$1}"
}
cgobuild(){
	CGO_CFLAGS="$CFLAGS" CGO_LDFLAGS="$LDFLAGS $GB_CGO_LDFLAGS" CGO_ENABLED=1 cross_in_cc gobuild "$@"
}
GB_CGO_LDFLAGS="-lopus -lm" cgobuild "yumeko" ""
cgobuild thumblink/thumblink
gobuild ymup
gobuild subhub
cp "$INSTALL_DIR/bin/ffmpeg" bin
echo "Build complete. thumblink and ffmpeg are nonfree and unredistributable."
