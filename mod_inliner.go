// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

//This is starting to look like a Java project or something
import (
	"bytes"
	"encoding/base64"
	"errors"
	"image/png"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"

	"bitbucket.org/abex/yumeko/thumblink/thumblinkrpc"
)

var inliner *thumblinkrpc.Inliner

//TryInlining inlines images, etc returning (true, updated string)
func TryInlining(input string) (string, error) {
	com, err := inliner.Inline(input)
	if err != nil {
		return input, err
	}
	msg := <-com
	if msg.Errors != "" {
		return msg.Msg, errors.New(msg.Errors)
	}
	return msg.Msg, nil
}

//StartupInliner starts up the inliner service
func StartupInliner() {
	var err error
	inliner, err = thumblinkrpc.MakeInliner()
	if err != nil {
		panic(err)
	}
}

//MakeQRCode turns a string(such as a url) into a <img> element with a QR code
func MakeQRCode(msg string) (string, error) {
	qrcode, err := qr.Encode(msg, qr.H, qr.Auto)
	if err != nil {
		return "", err
	}
	qrcode, err = barcode.Scale(qrcode, 200, 200)
	if err != nil {
		return "", err
	}
	var buf bytes.Buffer
	buf.Grow(2048)
	err = png.Encode(&buf, qrcode)
	if err != nil {
		return "", err
	}
	thumbdata := "data:image/png;base64," + base64.StdEncoding.EncodeToString(buf.Bytes())
	return "<a href='" + msg + "'><img src='" + thumbdata + "'/></a>", nil
}
