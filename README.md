Yumeko
======
Mumble bot.

Features
--------
- Web interface
- Soundboard
- Keep last 20* seconds of audio for download
- Image inlining
- Temp file storage

TODO
----
Docs

ShareX Destination
------
type `login sharex`


Command line uploader
---------------------
```
go get -u -v bitbucket.org/abex/yumeko/ymup
ymup --help
```