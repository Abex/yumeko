// +build ignore

package main

import (
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	fi, err := os.OpenFile("main.go", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0777)
	if err != nil {
		panic(err)
	}
	bytes, err := ioutil.ReadFile(os.Args[2])
	if err != nil {
		panic(err)
	}
	fi.WriteString(`package ` + os.Args[1] + `

//go:generate go run ../generate.go ` + strings.Join(os.Args[1:], " ") + `

import (
	"bitbucket.org/abex/yumeko/mime"
  "bytes"
)

func Include(db *mime.Database){
	db.ReadFreeDesktop(bytes.NewBufferString(` + strconv.Quote(string(bytes)) + `))
}
`)
}
