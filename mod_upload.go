// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"time"

	"bitbucket.org/abex/yumeko/commonConfig"
	"bitbucket.org/abex/yumeko/conf"

	"gopkg.in/mgo.v2/bson"
)

//Ratelimiter for all uploads
var UploadRateLimit = NewLimiterConf("upload", "5000", "30", "30")

//Ratelimiter for all blob downloads
var ServeRateLimit = NewLimiterConf("download", "100", "100", "500")

var uploadPath string
var uploadDefaultDuration int
var uploadMaxDuration int
var uploadPruneTick int

func init() {
	conf.Config(&uploadPath, "uploader", `
# Where to put upload data
path = "uploads/"
`)
	conf.Config(&uploadDefaultDuration, "uploader", `
# How many minutes to keep a uploaded file by default
default-duration = 360
`)
	conf.Config(&uploadMaxDuration, "uploader", `
# How many minutes can a user upload a file for.
# This is ignored for users with the UploadForever permission
max-duration = 2880
`)
	conf.Config(&uploadPruneTick, "uploader", `
# How often yumeko should remove expired uploads (in seconds)
prune-tick = 60
`)
}

//UploadItem is a uploaded item as stored in the DB
type UploadItem struct {
	Name       string    `bson:"Name"`
	ExpireDate time.Time `bson:"ExpireDate"`
	Uploader   uint32    `bson:"Uploader"`
	ID         string    `bson:"ID"`
}

//Path returns the path to the underlying file
func (i *UploadItem) Path() string {
	return path.Join(conf.Path(uploadPath), i.ID)
}

//Delete deletes the upload
func (i *UploadItem) Delete() error {
	err := Db.C("Upload").Remove(bson.M{"ID": i.ID})
	if err != nil {
		return err
	}
	err = os.Remove(i.Path())
	if err != nil {
		return err
	}
	body, err := json.Marshal(JSON{
		"ID": i.ID,
	})
	if err == nil {
		WSGlobalDispatch(&WSResponse{
			URI:    "/api/upload/cbDelete",
			Status: 200,
			Body:   string(body),
		})
	}
	return nil
}

//RemoveOldUploads removes old uploads from the upload collection
func RemoveOldUploads() {
	iter := Db.C("Upload").Find(bson.M{}).Iter()
	defer iter.Close()
	var item UploadItem
	for iter.Next(&item) {
		if item.ExpireDate.Before(time.Now()) {
			err := item.Delete()
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

func sendFile(w *Request, id string) {
	var item UploadItem
	err := Db.C("Upload").Find(bson.M{
		"ID": id,
	}).One(&item)
	if err != nil {
		w.Error(400, err)
		return
	}
	stat, err := os.Stat(item.Path())
	if err != nil {
		w.Error(500, err)
		return
	}
	file, err := os.Open(item.Path())
	if err != nil {
		w.Error(500, err)
		return
	}
	defer file.Close()
	ct := make([]byte, 512)
	l, _ := io.ReadFull(file, ct)
	typ := http.DetectContentType(ct[:l])
	w.W.Header().Set("Content-Type", typ)
	file.Seek(0, 0)
	w.W.Header().Set("Content-Disposition", "inline; filename=\""+item.Name+"\"")
	http.ServeContent(w.W, w.R, "", stat.ModTime(), file)
}

//StartUpload starts the file upload service
func StartUpload() {
	os.Mkdir(conf.Path(uploadPath), 0777)
	Handle("/api/upload/list", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			var r []UploadItem
			err := Db.C("Upload").Find(bson.M{}).All(&r)
			if err != nil {
				w.Error(500, err)
			}
			w.WriteJSON(200, r)
		}
	})
	Handle("/api/upload/upload", func(w *Request) {
		if w.IsWS() {
			return
		}
		if w.Limit(UploadRateLimit) {
			return
		}
		if err := w.R.ParseMultipartForm(0); err != nil {
			w.Error(500, err)
			return
		}
		defer w.R.MultipartForm.RemoveAll()
		user := w.GetUser()
		if user != nil {
			if !user.Has(PermissionFileUpload) {
				w.Error(401, errors.New("Missing permission"))
				return
			}
			file, info, err := w.R.FormFile("upload")
			if err != nil {
				w.Error(500, err)
				return
			}
			defer file.Close()

			expire := time.Duration(uploadDefaultDuration)
			expt, err := strconv.ParseInt(w.R.FormValue("expire"), 10, 32)
			if err == nil {
				if expt > int64(uploadMaxDuration) && !user.Has(PermissionFileUploadForever) {
					expire = time.Duration(uploadMaxDuration)
				} else {
					expire = time.Duration(expt)
				}
			}

			temp, err := ioutil.TempFile(conf.Path(uploadPath), "")
			if err != nil {
				w.Error(500, err)
				return
			}
			defer temp.Close()
			meta := UploadItem{
				Name:       info.Filename,
				ExpireDate: time.Now().Add(expire * time.Minute),
				Uploader:   user.ID,
				ID:         filepath.Base(temp.Name()),
			}
			err = Db.C("Upload").Insert(meta)
			if err != nil {
				w.Error(500, err)
				return
			}
			io.Copy(temp, file)
			body, err := json.Marshal(meta)
			if err == nil {
				WSGlobalDispatch(&WSResponse{
					URI:    "/api/upload/cbUpload",
					Status: 200,
					Body:   string(body),
				})
			}
			uri := fmt.Sprintf("%vapi/upload/download/%v/%v", commonConfig.ExternalHostname, meta.ID, url.QueryEscape(meta.Name))
			w.WriteJSON(200, JSON{
				"success": true,
				"path":    uri,
			})
			if ParseBool(w.R.FormValue("inline"), false) {
				go func() {
					str, _ := TryInlining(`<a href="` + uri + `"></a>`)
					muser := user.M()
					cha := Mumble.Self.Channel
					if muser != nil {
						cha = muser.Channel
					}
					cha.Send(user.ChatName()+str, false)
				}()
			}
		}
	})
	Handle("/api/upload/download", func(w *Request) {
		if w.Limit(ServeRateLimit) {
			return
		}
		w.R.ParseForm()
		id := w.R.FormValue("id")
		sendFile(w, id)
	})
	downloadRegexp := regexp.MustCompile(`\/api\/upload\/download\/([^\/]+)\/?`)
	Handle("/api/upload/download/", func(w *Request) {
		sm := downloadRegexp.FindStringSubmatch(w.R.URL.Path)
		if len(sm) != 2 {
			w.Error(400, errors.New("Missing ID"))
			return
		}
		sendFile(w, sm[1])
	})
	Handle("/api/upload/delete", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			if !user.Has(PermissionFileUploadDelete) {
				w.Error(403, errors.New("Missing Permission"))
				return
			}
			id := w.R.FormValue("id")
			var item UploadItem
			err := Db.C("Upload").Find(bson.M{
				"ID": id,
			}).One(&item)
			if err != nil {
				w.Error(400, err)
				return
			}
			err = item.Delete()
			if err != nil {
				w.Error(500, err)
				return
			}
			w.WriteJSON(200, JSON{
				"success": true,
			})
		}
	})
	go func() {
		for {
			RemoveOldUploads()
			time.Sleep(time.Duration(uploadPruneTick) * time.Second)
		}
	}()
}
