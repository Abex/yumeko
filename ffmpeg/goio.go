// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package ffmpeg

// #include "libavformat/avio.h"
import "C"

import (
	"fmt"
	"io"
	"reflect"
	"runtime/debug"
	"unsafe"
)

var goreadmap = map[uintptr]*goiodata{}

func setup(ugodata unsafe.Pointer, bbuf *C.uint8_t, clength C.int) *goiodata {
	goiodata := goreadmap[uintptr(ugodata)]
	ibuf := uintptr(unsafe.Pointer(bbuf))
	ilen := int(clength)
	goiodata.bufhead.Data = ibuf
	goiodata.bufhead.Len = ilen
	goiodata.bufhead.Cap = ilen
	return goiodata
}

func ccheck() {
	err := recover()
	if err != nil {
		fmt.Printf("goread: recovered \n%v\n", err)
		fmt.Printf("Stack:%s\n", debug.Stack())
	}
}

//export goread
func goread(ugodata unsafe.Pointer, bbuf *C.uint8_t, clength C.int) C.int {
	defer ccheck()
	goiodata := setup(ugodata, bbuf, clength)
	read, _ := io.ReadFull(goiodata.reader, goiodata.buf)
	return C.int(read)
}

//export goseek
func goseek(ugodata unsafe.Pointer, offset C.int64_t, whence C.int) C.int64_t {
	defer ccheck()
	goiodata := goreadmap[uintptr(ugodata)]
	var mode int
	switch whence {
	case C.SEEK_SET:
		mode = io.SeekStart
	case C.SEEK_CUR:
		mode = io.SeekCurrent
	case C.SEEK_END:
		mode = io.SeekEnd
	}
	nof, _ := goiodata.seeker.Seek(int64(offset), mode)
	return C.int64_t(nof)
}

type goiodata struct {
	reader  io.Reader
	seeker  io.Seeker
	buf     []byte
	bufhead *reflect.SliceHeader
}

func mkgoio(reader io.Reader, key uintptr) bool {
	seeker, ok := reader.(io.Seeker)
	godata := &goiodata{
		reader: reader,
		seeker: seeker,
	}
	godata.bufhead = (*reflect.SliceHeader)(unsafe.Pointer(&godata.buf))
	goreadmap[key] = godata
	return ok
}

//export goread_free
func goread_free(ugodata uintptr) {
	delete(goreadmap, ugodata)
}
