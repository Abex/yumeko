// Copyright 2016 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file
 
#ifndef __GOIO_H_GUARD
#define __GOIO_H_GUARD

int goread(void*,uint8_t*,int);
int64_t goseek(void*,int64_t,int);
void goread_free(void*);

#endif //__GOIO_H_GUARD