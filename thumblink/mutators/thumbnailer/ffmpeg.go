// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumbnailer

import (
	"bitbucket.org/abex/yumeko/ffmpeg"
	"bitbucket.org/abex/yumeko/thumblink"
)

func ReadFFmpeg(body *thumblink.Peekable, animMode AnimMode, mime string, maxWidth, maxHeight int) (ffmpeg.Frame, int, AnimMode, error) {
	//Open the demuxer (lav format)
	var fc ffmpeg.FormatCtx
	var err error
	orient := 0
	if mime == "image/jpeg" {
		orient, err = findOrientation(body.Peeker())
		if err != nil {
			return 0, orient, animMode, err
		}
	}
	if mime == "image/gif" {
		fc, err = ffmpeg.OpenFormatStream(body.Peeker())
	} else {
		fc, err = ffmpeg.OpenFormatStream(body)
	}
	defer fc.Free()

	if err != nil {
		return 0, orient, animMode, err
	}
	videoID, err := fc.FindStream(ffmpeg.MediaTypeVideo)
	if err != nil {
		return 0, orient, animMode, err
	}

	if animMode == AnimModeGuess { // If the audio track is before the video track its probably music, not a video per se
		audioID, _ := fc.FindStream(ffmpeg.MediaTypeAudio)
		if audioID != -1 && audioID < videoID {
			animMode = AnimModeAudio
		}
	}
	//Discard irrelivant streams
	fc.SetDiscard()
	//Init our stream
	cc, err := fc.GetCodec(videoID)
	defer cc.Free()
	if err != nil {
		return 0, orient, animMode, err
	}

	//Guess the size of the frame, and if the codec supports downscaling while decoding
	{
		width, height, maxLowres := cc.GetSizeInfo()
		lr := 0
		for ; lr < maxLowres; lr++ { // lowres devides the size of the frame by 2^lowres. Find the optimal value
			div := 1 << uint(lr)
			if width/div < maxWidth || height/div < maxHeight {
				lr--
				break
			}
		}
		if lr > 0 {
			cc.SetLowres(lr)
		}
	}
	err = cc.Open() //Finish initing after lowres is set
	if err != nil {
		return 0, orient, animMode, err
	}

	//Alloc a AVFrame to store our output
	frm, err := ffmpeg.MakeFrame()
	if err != nil {
		return 0, orient, animMode, err
	}

	// +Read Frames
	err = func() error {
		//And a 'testing' frame
		bframe, err := ffmpeg.MakeFrame()
		if err != nil {
			return err
		}
		defer func() { bframe.Free() }() //in a closure so its pass by reference, not value (so we dont free the wrong frame)

		i := 0
		for i < 60 {
			err = cc.ReadFrame(bframe)
			if err != nil {
				break
			}
			frm, bframe = bframe, frm
			i++
		}
		if i < 1 { //no good frame
			return err
		}

		if animMode == AnimModeGuess {
			if i > 1 { // multiple frames == video
				animMode = AnimModeVideo
			} else {
				animMode = AnimModeNormal
			}
		}
		return nil
	}() // -ReadFrames
	if err != nil {
		frm.Free()
		return 0, orient, animMode, err
	}
	return frm, orient, animMode, nil
}

func ffReadFrame(name string) (ffmpeg.Frame, error) {
	fc, err := ffmpeg.OpenFormatFile(name)
	defer fc.Free()
	if err != nil {
		return 0, err
	}
	videoStream, err := fc.FindStream(ffmpeg.MediaTypeVideo)
	if err != nil {
		return 0, err
	}
	cc, err := fc.GetCodec(videoStream)
	defer cc.Free()
	if err != nil {
		return 0, err
	}
	err = cc.Open()
	if err != nil {
		return 0, err
	}
	frm, err := ffmpeg.MakeFrame()
	if err != nil {
		return 0, err
	}
	err = cc.ReadFrame(frm)
	if err != nil {
		frm.Free()
		return 0, err
	}
	return frm, nil
}
