// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package reddit transforms reddit threads so they are inlined
package reddit

import (
	"encoding/json"
	"html"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

//Mutator mutates reddit links so they are thumbnailed properly
type Mutator struct{}

var commentMatcher = regexp.MustCompile(`/r/[^/]*/comments/.*`)

//Mutate impl thumblink.Mutator
func (_ Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	out := make([]thumblink.HTMLAble, 0, len(in))
	var errors []error
	for _, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			url := (*url.URL)(link)
			if link.HostMatch("reddit.com") && commentMatcher.MatchString(url.Path) {
				nout, nerr := inlineReddit(url)
				if nout != nil {
					out = append(out, nout...)
				} else {
					nout = append(out, link)
				}
				if nerr != nil {
					errors = append(errors, nerr)
				}
				continue
			}
			out = append(out, link)
		default:
			out = append(out, link)
		}
	}
	return out, errors
}

type reply struct {
	Data struct {
		Children []struct {
			Kind string `json:"kind"`
			Data struct {
				Subreddit     string `json:"subreddit"`
				SelfText      string `json:"selftext"`
				LinkFlairText string `json:"link_flair_text"`
				Author        string `json:"author"`
				Title         string `json:"title"`
				Body          string `json:"body"`
				URL           string `json:"url"`
				Permalink     string `json:"permalink"`
			} `json:"data"`
		} `json:"children"`
	} `json:"data"`
}

func inlineReddit(uurl *url.URL) ([]thumblink.HTMLAble, error) {
	saurl := *uurl
	aurl := &saurl
	aurl.Host = "api.reddit.com"
	aurl.Scheme = "https"
	aurl.RawQuery = "raw_json=1&limit=1"
	req, err := http.NewRequest("GET", aurl.String(), nil)
	if err != nil {
		return nil, err
	}
	versioning.SetUserAgent(req)
	res, err := util.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	va := []reply{}
	err = json.NewDecoder(res.Body).Decode(&va)
	if err != nil {
		return nil, err
	}
	vc := va[0]
	if len(va) > 1 && strings.Count(uurl.Path[:len(uurl.Path)-1], "/") > 5 { // trim trailing char(for trailing slash) /r/subreddit/comments/link_id/nice/comment_id/
		vc = va[1]
	}
	v := vc.Data.Children[0]
	d := v.Data
	lh := []thumblink.HTMLAble{
		thumblink.HTML{
			`<a href="https://www.reddit.com/user/`,
			html.EscapeString(d.Author),
			`">`,
			html.EscapeString(d.Author),
			`</a>: `,
		},
	}
	title := d.Title
	if title == "" {
		title = uurl.String()
	}
	lh = append(lh, thumblink.HTML{
		`<a href="`,
		html.EscapeString(uurl.String()),
		`">`,
		html.EscapeString(title),
		`</a>`,
	})
	if d.LinkFlairText != "" {
		lh = append(lh, thumblink.HTML{
			` [`,
			html.EscapeString(d.LinkFlairText),
			`]`,
		})
	}
	body := d.Body
	if body == "" {
		body = d.SelfText
	}
	if body != "" {
		h := thumblink.HTML{`<p>`}
		lines := strings.Split(body, "\n")
		for _, line := range lines {
			h = append(h, line, `<br/>`)
		}
		h[len(h)-1] = `</p>`
		if len(h) > 2 {
			lh = append(lh, h)
		}
	}
	if !strings.HasSuffix(d.URL, d.Permalink) {
		l, _ := url.Parse(d.URL)
		if l != nil {
			lh = append(lh, (*thumblink.Link)(l))
		}
	}
	return lh, nil
}

func (_ Mutator) Help() string {
	return "reddit.com posts."
}
