// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package twitter transforms twitter so they are inlined
package twitter

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"html"
	"math"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

//Mutator mutates twitter links so they are thumbnailed properly
type Mutator struct {
}

var key string
var secret string
var token string

func init() {
	conf.Config(&key, "keys.twitter", `
# Twitter Consumer Key
# https://apps.twitter.com/
key = ""
`)
	conf.Config(&secret, "keys.twitter", `
# Twitter Consumer Secret
secret = ""
`)
}

const apiRoot = "https://api.twitter.com/"

func login() error {
	req, err := http.NewRequest("POST", apiRoot+"oauth2/token", bytes.NewBufferString(`grant_type=client_credentials`))
	if err != nil {
		return err
	}
	b64 := base64.URLEncoding.EncodeToString(([]byte)(key + ":" + secret))
	req.Header.Add("Authorization", "Basic "+b64)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	versioning.SetUserAgent(req)
	res, err := util.Client.Do(req)
	if err != nil {
		return err
	}
	tokres := struct {
		AccessToken string `json:"access_token"`
	}{}
	err = json.NewDecoder(res.Body).Decode(&tokres)
	res.Body.Close()
	if err != nil {
		return err
	}
	token = "Bearer " + tokres.AccessToken
	return nil
}

type indexable interface {
	Index() [2]int
	HTMLAble() (thumblink.HTMLAble, []string)
	UUID() string
}
type indexes []indexable

func (i indexes) Len() int {
	return len(i)
}
func (i indexes) Less(a, b int) bool {
	return i[a].Index()[0] < i[b].Index()[0]
}
func (i indexes) Swap(a, b int) {
	i[a], i[b] = i[b], i[a]
}

type userMentions struct {
	ScreenName string `json:"screen_name"`
	Indices    [2]int `json:"indices"`
}

func (u userMentions) Index() [2]int {
	return u.Indices
}
func (u userMentions) HTMLAble() (thumblink.HTMLAble, []string) {
	ll := html.EscapeString(u.ScreenName)
	return nil, []string{"<a href=\"https://twitter.com/", ll, "\">@", ll, "</a>"}
}
func (u userMentions) UUID() string {
	return fmt.Sprint(u.Indices)
}

type eURL struct {
	URL     string `json:"expanded_url"`
	Indices [2]int `json:"indices"`
}

func (u eURL) Index() [2]int {
	return u.Indices
}
func (u eURL) HTMLAble() (thumblink.HTMLAble, []string) {
	return thumbnailer.Image{
		URL:   u.URL,
		Flags: thumbnailer.ImageFlagHideError,
	}, nil
}

func (u eURL) UUID() string {
	return u.URL
}

type media struct {
	URL     string `json:"media_url_https"`
	UserURL string `json:"expanded_url"`
	Indices [2]int `json:"indices"`
	Type    string `json:"type"`
}

func (u media) Index() [2]int {
	return u.Indices
}
func (u media) HTMLAble() (thumblink.HTMLAble, []string) {
	mode := thumbnailer.AnimModeGuess
	if u.Type == "video" || strings.Contains(u.URL, "tweet_video_thumb") {
		mode = thumbnailer.AnimModeVideo
	}
	return thumbnailer.Image{
		ImageURL: u.URL,
		URL:      u.UserURL,
		Flags:    thumbnailer.ImageFlagHideError,
		Animated: mode,
	}, nil
}

func (u media) UUID() string {
	return u.URL
}

type entities struct {
	UserMentions []userMentions `json:"user_mentions"`
	URLs         []eURL         `json:"urls"`
	Media        []media        `json:"media"`
}

func flatten(e entities) []indexable {
	out := make([]indexable, len(e.UserMentions)+len(e.URLs)+len(e.Media))
	i := 0
	for _, v := range e.UserMentions {
		out[i] = v
		i++
	}
	for _, v := range e.URLs {
		out[i] = v
		i++
	}
	for _, v := range e.Media {
		out[i] = v
		i++
	}
	return out
}

type status struct {
	Errors []struct {
		Code    int    `json:"code"`
		Label   string `json:"label"`
		Message string `json:"message"`
	} `json:"errors"`
	Text             string   `json:"full_text"`
	Time             string   `json:"created_at"`
	Entities         entities `json:"entities"`
	ExtendedEntities entities `json:"extended_entities"`
	User             struct {
		Name       string `json:"name"`
		ScreenName string `json:"screen_name"`
		//Image      string `json:"profile_image_url_https"`
	} `json:"user"`
}

func inline(id, url string, recur bool) ([]thumblink.HTMLAble, error) {
	req, err := http.NewRequest("GET", apiRoot+"1.1/statuses/show.json?tweet_mode=extended&id="+id, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", token)
	versioning.SetUserAgent(req)
	res, err := util.Client.Do(req)
	if err != nil {
		return nil, err
	}
	api := status{}
	err = json.NewDecoder(res.Body).Decode(&api)
	res.Body.Close()
	if err != nil {
		return nil, err
	}
	if len(api.Errors) > 0 {
		es := make([]error, 0, len(api.Errors))
		for _, v := range api.Errors {
			if v.Code == 99 && recur {
				err = login()
				if err != nil {
					return nil, err
				}
				return inline(id, url, true)
			}
			es = append(es, fmt.Errorf("Twitter (%v): %v", v.Code, v.Message))
		}
		if len(es) > 1 {
			return nil, glug.Combine(es)
		}
		return nil, es[0]
	}
	ts := api.Time
	tm, err := time.Parse("Mon Jan 2 15:04:05 -0700 2006", ts)
	if err == nil {
		td := time.Now().Sub(tm)
		if td < time.Hour {
			if td < time.Minute {
				ts = fmt.Sprintf("%v seconds ago", math.Floor(td.Seconds()+.5))
			} else {
				ts = fmt.Sprintf("%v minutes ago", math.Floor(td.Minutes()+.5))
			}
		} else {
			if td < time.Hour*12 {
				ts = tm.In(time.Local).Format("15:04:05")
			} else {
				ts = tm.In(time.Local).Format("01/02/06 15:04:05")
			}
		}
	}
	ois := indexes(append(flatten(api.ExtendedEntities), flatten(api.Entities)...))
	is := ois[:0]
	lm := map[string]struct{}{}
	for _, v := range ois {
		uuid := v.UUID()
		_, ok := lm[uuid]
		if !ok {
			lm[uuid] = struct{}{}
			is = append(is, v)
		}
	}
	sort.Sort(is)
	ret := []thumblink.HTMLAble{}
	html := thumblink.HTML{
		`<a href="`, html.EscapeString(url), `">`,
		api.User.Name, " ",
		ts, `:</a><p>`,
	}
	index := 0
	text := []rune(api.Text)
	for _, v := range is {
		vidx := v.Index()
		if vidx[0] > index {
			html = append(html, newlineReplacer.Replace(string(text[index:vidx[0]])))
		}
		if vidx[1] > index {
			index = vidx[1]
		}
		nh, ns := v.HTMLAble()
		if nh != nil {
			if len(html) > 0 {
				ret = append(ret, html)
				html = thumblink.HTML{}
			}
			ret = append(ret, nh)
		} else {
			html = append(html, ns...)
		}
	}
	if index < len(text) {
		html = append(html, newlineReplacer.Replace(string(text[index:])))
	}
	html = append(html, `</p>`)
	ret = append(ret, html)

	return ret, nil
}

var newlineReplacer = strings.NewReplacer("\r", "", "\n", "<br/>")

func deshorten(l *thumblink.Link) (*thumblink.Link, error) {
	ul := (*url.URL)(l)
	req, err := http.NewRequest("HEAD", ul.String(), nil)
	if err != nil {
		return nil, err
	}
	versioning.SetUserAgent(req)
	res, err := util.NoRedirectClient.Do(req)
	if err != nil {
		return l, err
	}
	if res.Body != nil {
		res.Body.Close()
	}
	nl := res.Header.Get("Location")
	n, err := url.Parse(nl)
	if err != nil {
		return l, err
	}
	return (*thumblink.Link)(n), nil
}

//Mutate impl thumblink.Mutator
func (m Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	out := make([]thumblink.HTMLAble, 0, len(in))
	if token == "" {
		err := login()
		if err != nil {
			return in, []error{err}
		}
	}
	var errors []error
	for _, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			if link.HostMatch("t.co") {
				var err error
				link, err = deshorten(link)
				if err != nil {
					errors = append(errors, err)
				}
			}
			url := (*url.URL)(link)
			if link.HostMatch("twitter.com") && len(url.Path) > 11 {
				idx := strings.IndexByte(link.Path[1:], '/')
				if len(url.Path) > idx+10 && url.Path[idx+1:idx+9] == "/status/" {
					id := url.Path[idx+9:]
					eidx := strings.IndexByte(id, '/')
					if eidx > 0 {
						id = id[:eidx]
					}
					app, err := inline(id, url.String(), false)
					if err != nil {
						errors = append(errors, glug.Wrap(err, link.String()))
					}
					if len(app) > 0 {
						out = append(out, app...)
					} else {
						out = append(out, link)
					}
					continue
				}
			}
			out = append(out, link)
		default:
			out = append(out, mut)
		}
	}
	return out, errors
}

func (_ Mutator) Help() string {
	return "twitter.com tweets and t.co links."
}
