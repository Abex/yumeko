// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

//This is starting to look like a Java project or something
import (
	"bytes"
	"encoding/base64"
	"fmt"
	"html"
	"image/png"
	"sort"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/fourChan"
	"bitbucket.org/abex/yumeko/thumblink/mutators/gfycat"
	"bitbucket.org/abex/yumeko/thumblink/mutators/google"
	"bitbucket.org/abex/yumeko/thumblink/mutators/imgur"
	"bitbucket.org/abex/yumeko/thumblink/mutators/reddit"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
	"bitbucket.org/abex/yumeko/thumblink/mutators/twitch"
	"bitbucket.org/abex/yumeko/thumblink/mutators/twitter"
	"bitbucket.org/abex/yumeko/thumblink/mutators/vine"
	"bitbucket.org/abex/yumeko/thumblink/mutators/wiki"
	"bitbucket.org/abex/yumeko/thumblink/mutators/youtube"
	"bitbucket.org/abex/yumeko/versioning"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
)

var mutators = []thumblink.Mutator{}
var sm = &SpoilerMutator{""}
var enableHelp bool

func init() {
	conf.Config(&enableHelp, "inliner.help", `
# Convert 'help inliner' to print a help string
enabled = true
`)
	configMutator(thumblink.Prefix{"@", sm}, "inliner.spoiler", `
# Prefix with '@' to replace the link with the spoiler image
enabled = true
`)
	configMutator(thumblink.Prefix{"$", HTMLerMutator{}}, "inliner.disable", `
# Prefix with '$' to disable the inliner for that link
enabled = true
`)
	configMutator(thumblink.Prefix{"#", QRMutator{}}, "inliner.QR", `
# Prefix with '#' to replace the link with a QR code
enabled = true
`)
	configMutator(reddit.Mutator{}, "inliner.reddit", `
enabled = true
`)
	configMutator(google.Mutator{}, "inliner.google", `
enabled = true
`)
	configMutator(twitter.Mutator{}, "inliner.twitter", `
enabled = false
`)
	configMutator(youtube.Mutator{}, "inliner.youtube", `
enabled = false
`)
	configMutator(gfycat.Mutator{}, "inliner.gfycat", `
enabled = true
`)
	configMutator(vine.Mutator{}, "inliner.vine", `
enabled = false
`)
	configMutator(imgur.Mutator{}, "inliner.imgur", `
enabled = false
`)
	configMutator(twitch.Mutator{}, "inliner.twitch", `
enabled = false
`)
	configMutator(fourChan.Mutator{}, "inliner.4chan", `
enabled = true
`)
	configMutator(wiki.Mutator{}, "inliner.wiki", `
enabled = true
`)
}

func startup() {
	thumbnailer.Startup()
	conf.OpenFileDefault("spoiler.txt", &sm.SpoilerText, `Edit this in spoiler.txt<br> As usual its mumble's reduced HTML.`)
	mutators = append(mutators, thumbnailer.Mutator{})
}

func configMutator(mut thumblink.Mutator, name, def string) {
	en := false
	conf.Config(&en, name, def)
	mutators = append(mutators, opmut{
		Enable: &en,
		Mut:    mut,
	})
}

type opmut struct {
	Enable *bool
	Mut    thumblink.Mutator
}

func (o opmut) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	if *o.Enable {
		return o.Mut.Mutate(in)
	}
	return in, nil
}

func (o opmut) Help() string {
	if *o.Enable {
		return o.Mut.Help()
	}
	return ""
}

func PrintVersion() (string, error) {
	return fmt.Sprintf("%v</br>", versioning.LongVersion()), nil
}

func PrintHelp() (string, error) {
	helps := make([]string, 0, len(mutators))
	for _, mut := range mutators {
		helpstr := mut.Help()
		if helpstr != "" {
			helps = append(helps, helpstr)
		}
	}
	sort.Strings(helps)
	buf := &bytes.Buffer{}
	for _, helpstr := range helps {
		buf.WriteString(helpstr)
		buf.WriteString(`<br/>`)
	}
	return buf.String(), nil
}

//TryInlining inlines images, etc returning (true, updated string)
func inline(input string) (string, error) {
	if enableHelp {
		if input == "thumblink version" {
			return PrintVersion()
		} else if input == "thumblink help" {
			return PrintHelp()
		}
	}
	segs, err := thumblink.FindLinks(input)
	if err != nil {
		panic(err)
	}
	for _, s := range segs {
		if _, ok := s.(*thumblink.Link); ok {
			goto have
		}
	}
	return "", nil
have:

	var errors []error

	for _, mut := range mutators {
		var err []error
		segs, err = mut.Mutate(segs)
		if err != nil {
			errors = append(errors, err...)
		}
	}

	output := thumblink.Join(segs)
	err = glug.Combine(errors)

	return output, err
}

//MakeQRCode turns a string(such as a url) into a <img> element with a QR code
func MakeQRCode(msg string) (string, error) {
	qrcode, err := qr.Encode(msg, qr.H, qr.Auto)
	if err != nil {
		return "", err
	}
	qrcode, err = barcode.Scale(qrcode, 200, 200)
	if err != nil {
		return "", err
	}
	var buf bytes.Buffer
	buf.Grow(2048)
	err = png.Encode(&buf, qrcode)
	if err != nil {
		return "", err
	}
	thumbdata := "data:image/png;base64," + base64.StdEncoding.EncodeToString(buf.Bytes())
	return "<a href='" + msg + "'><img src='" + thumbdata + "'/></a>", nil
}

type SpoilerMutator struct {
	SpoilerText string
}

func (s *SpoilerMutator) Mutate(link *thumblink.Link) ([]thumblink.HTMLAble, []error) {
	return []thumblink.HTMLAble{thumblink.HTML([]string{"<a href=\"", html.EscapeString(link.String()), "\">", s.SpoilerText, "</a>"})}, nil
}

func (s *SpoilerMutator) Help() string {
	return "replace the link with the spoiler image"
}

type QRMutator struct{}

func (_ QRMutator) Mutate(link *thumblink.Link) ([]thumblink.HTMLAble, []error) {
	s, err := MakeQRCode(link.String())
	if err != nil {
		return []thumblink.HTMLAble{link}, []error{err}
	}
	return []thumblink.HTMLAble{thumblink.HTML([]string{s})}, nil
}

func (_ QRMutator) Help() string {
	return "turn the link into a QR code"
}

type HTMLerMutator struct{}

func (_ HTMLerMutator) Mutate(link *thumblink.Link) ([]thumblink.HTMLAble, []error) {
	return []thumblink.HTMLAble{thumblink.HTML(link.HTML())}, nil
}

func (_ HTMLerMutator) Help() string {
	return "do nothing to the link"
}
