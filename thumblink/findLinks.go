// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumblink

import (
	"bytes"
	"io"
	"net/url"

	"golang.org/x/net/html"
)

// FindLinks traverses HTML in input, making a array of HTMLAble
func FindLinks(input string) ([]HTMLAble, error) {
	output := []HTMLAble{}

	tokStream := html.NewTokenizer(bytes.NewBufferString(input))
	underADepth := 0
	for {
		write := true
		tokType := tokStream.Next()
		switch tokType {
		case html.ErrorToken:
			err := tokStream.Err()
			if err != io.EOF {
				return nil, err
			}
			return output, nil
		case html.StartTagToken:
			if underADepth > 0 {
				underADepth++
			} else {
				name, hasAttr := tokStream.TagName()
				if hasAttr && bytes.Equal(name, []byte{'a'}) {
					var href *url.URL
					ok := false
					for {
						key, val, more := tokStream.TagAttr()
						if bytes.Equal(key, ([]byte)("href")) {
							var err error
							href, err = url.Parse(string(val))
							if err == nil {
								ok = true
							}
							break
						}
						if !more {
							break
						}
					}
					if ok && href != nil {
						write = false
						output = append(output, (*Link)(href))
						underADepth = 1
					}
					break
				}
			}
		case html.EndTagToken:
			if underADepth > 0 {
				underADepth--
				write = false
			}
		}
		if underADepth == 0 && write {
			if tokType == html.TextToken {
				output = append(output, Text([]string{string(tokStream.Raw())}))
			} else {
				output = append(output, Tag(tokStream.Raw()))
			}
		}
	}
}
