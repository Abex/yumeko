// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumblink

import (
	"fmt"
	"regexp"
)

const (
	rpFound int = iota
	rpNotEnough
	rpFailed
)

//RemovePostfix checks is haystack is ended by needle, ignoring spaces, tabs, newlines and carrage returns. If it is found it is removed
func removePostfix(needle string, haystacks []string) (int, []string) {
	for ai := len(haystacks) - 1; ai >= 0; ai-- {
		haystack := haystacks[ai]
		for i := len(haystack) - len(needle); i >= 0; i-- {
			for ni := 0; ni < len(needle); ni++ {
				if needle[ni] != haystack[ni+i] {
					goto mismatch
				}
			}
			//match
			return rpFound, append(haystacks[:ai], haystack[:i])
		mismatch:
			switch haystack[i] {
			case ' ', '\n', '\r', '\t':
				continue
			default:
				return rpFailed, haystacks
			}
		}
	}
	return rpNotEnough, haystacks
}

type Prefix struct {
	Prefix  string
	Mutator PrefixMutator
}

type PrefixMutator interface {
	Mutate(link *Link) ([]HTMLAble, []error)
	Help() string
}

var prefixMatcher = regexp.MustCompile("(T+)(?:H{0,2})(L)")

func prep(in []HTMLAble) []byte {
	mid := make([]byte, len(in))
	for i := range in {
		switch in[i].(type) {
		case *Link:
			mid[i] = 'L'
		case Text:
			mid[i] = 'T'
		case Tag:
			mid[i] = 'H'
		default:
			mid[i] = 'O'
		}
	}
	return mid
}

func (p Prefix) Mutate(in []HTMLAble) ([]HTMLAble, []error) {
	var errors []error
	out := make([]HTMLAble, 0, len(in))
	mid := prep(in)

	matches := prefixMatcher.FindAllSubmatchIndex(mid, -1)
	if len(matches) == 0 {
		return in, nil
	}
	lastMatch := 0
match:
	for _, match := range matches {
		if len(match) != 6 {
			continue
		}
		for i := match[3] - 1; i >= match[2]; i-- {
			ok, newstr := removePostfix(p.Prefix, in[i].HTML())
			if ok == rpFailed {
				continue match
			}
			if lastMatch < i {
				out = append(out, in[lastMatch:i]...)
			}
			lastMatch = i
			out = append(out, Text(newstr))
			if ok == rpFound {
				goto found
			}
		}
		continue match
	found:
		i := match[4]
		out = append(out, in[lastMatch:i]...)
		lastMatch = i + 1
		l, err := p.Mutator.Mutate(in[i].(*Link))
		if err != nil {
			errors = append(errors, err...)
		}
		out = append(out, l...)
	}
	out = append(out, in[lastMatch:]...)
	return out, errors
}

func (p Prefix) Help() string {
	return fmt.Sprintf("Prefix with '%v' to %v.", p.Prefix, p.Mutator.Help())
}
