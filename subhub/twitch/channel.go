// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package twitch

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

type User struct {
	ID          string `json:"_id"`
	Bio         string `json:"bio"`
	DisplayName string `json:"display_name"`
	Logo        string `json:"logo"`
	Name        string `json:"name"`
	Type        string `json:"type"`
}

func GetUsers(key string, names []string) ([]User, error) {
	users := struct {
		Users []User `json:"users"`
	}{}
	url, err := url.Parse("https://api.twitch.tv/kraken/users")
	if err != nil {
		panic(err)
	}
	q := url.Query()
	for _, name := range names {
		q.Add("login", name)
	}
	url.RawQuery = q.Encode()
	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}
	versioning.SetUserAgent(req)
	req.Header.Add("Accept", "application/vnd.twitchtv.v5+json")
	req.Header.Add("Client-ID", key)
	res, err := util.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&users)
	return users.Users, err
}

type Channel struct {
	Status      string `json:"status"`
	DisplayName string `json:"display_name"`
	Game        string `json:"game"`
	Logo        string `json:"logo"`
	URL         string `json:"url"`
}

func GetChannel(key, name string) (Channel, error) {
	c := Channel{}
	users, err := GetUsers(key, []string{name})
	if err != nil {
		return c, err
	}
	if len(users) <= 0 {
		return c, fmt.Errorf("Cannot find user by name %q", name)
	}
	req, err := http.NewRequest("GET", "https://api.twitch.tv/kraken/channels/"+users[0].ID, nil)
	if err != nil {
		return c, err
	}
	versioning.SetUserAgent(req)
	req.Header.Add("Accept", "application/vnd.twitchtv.v5+json")
	req.Header.Add("Client-ID", key)
	res, err := util.Client.Do(req)
	if err != nil {
		return c, err
	}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&c)
	return c, err
}
