// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"bytes"
	"fmt"
	"html"
	"io/ioutil"
	"os"
	"strings"

	"bitbucket.org/abex/yumeko/subhub/twitch"
	"bitbucket.org/abex/yumeko/util"
)

//This is a temporary way to add this feature.
//It will eventually be implemented with proper configs and intergation
// For now you need a file with the following lines
// - Host
// - Service key for twitch
// - Twitch "Client-ID" - https://www.twitch.tv/settings/connections > "Developer Applications"
// - any number of twitch channels on seperate lines
func main() {
	byt, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(byt), "\n")
	host := lines[0]
	servicekey := lines[1]
	apikey := lines[2]
	conn, err := twitch.MakeConnection(apikey)
	if err != nil {
		panic(err)
	}
	for _, ichannel := range lines[3:] {
		channel := ichannel //closure in loop
		conn.ListenPlayback(channel, twitch.PlaybackHandlerClosures{
			ErrorHandler: func(err error) {
				fmt.Printf("Error: %v\n", err)
			},
			UpHandler: func() {
				channel = html.EscapeString(channel)
				util.Client.Post(host+"/api/chat?key="+servicekey, "text/html", bytes.NewBufferString(`<a href="https://www.twitch.tv/`+channel+`">`+channel+`</a>`))
				fmt.Printf("%v has went up!\n", channel)
			},
			DownHandler: func() {
				fmt.Printf("%v has went down!\n", channel)
			},
			ViewCountHandler: func(vc int) {
				fmt.Printf("%v has %v viewers!\n", channel, vc)
			},
			CommercialHandler: func() {
				fmt.Printf("%v is showing a commercial", channel)
			},
		})
	}
	<-make(chan struct{})
}
