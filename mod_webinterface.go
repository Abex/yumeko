// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"

	"bitbucket.org/abex/yumeko/conf"

	"gopkg.in/mgo.v2/bson"
)

//SendPoints updates the client's points value
func (u *User) SendPoints() {
	if u.Service {
		return
	}
	body, err := json.Marshal(JSON{
		"points": u.Points,
	})
	if err == nil {
		u.SendWS(&WSResponse{
			URI:    "/api/points",
			Status: 200,
			Body:   string(body),
		})
	}
}

//SendFullUpdate updates the all the client's value BUT points
func (u *User) SendFullUpdate(isDisconnecting bool) {
	if u.Service {
		return
	}
	c := u.ToDisk().ToClient()
	if isDisconnecting {
		c["Connected"] = false
	}
	body, err := json.Marshal(c)
	if err == nil {
		WSGlobalDispatch(&WSResponse{
			URI:    "/api/cbUserUpdate",
			Status: 200,
			Body:   string(body),
		})
	}
}

var webPublic string
var grecaptchaSiteKey string
var webCaptchaPoints uint

func init() {
	conf.Config(&webPublic, "misc", `
# Location of the web ui files
public = "public/"
`)
	conf.Config(&webCaptchaPoints, "points", `
# How many points should you get for filling a captcha
captcha-points = 5
`)
	conf.Config(&grecaptchaSiteKey, "keys.grecaptcha", `
# Site key for Google ReCaptcha
# Get a key pair at: 
# https://developers.google.com/recaptcha/docs/start
site-key = ""
`)
}

//StartWebInterface starts the web interface
func StartWebInterface() {
	Mux.Handle("/", http.FileServer(http.Dir(conf.Path(webPublic))))
	Handle("/api/user", func(r *Request) {
		user := r.GetUser()
		if user != nil {
			users := JSON{}
			var u UserDisk
			iter := Db.C("UserData").Find(bson.M{}).Iter()
			for iter.Next(&u) {
				if u.ID == Mumble.Self.UserID {
					continue
				}
				users[strconv.FormatUint(uint64(u.ID), 10)] = u.ToClient()
			}
			super := FindUser(0)
			if super != nil {
				users["0"] = Load(0, false).ToDisk().ToClient()
			}
			iter.Close()
			r.WriteJSON(200, JSON{
				"ID":      user.ID,
				"Users":   users,
				"success": true,
			})
		}
	})
	Handle("/api/config", func(r *Request) {
		r.WriteJSON(200, JSON{
			"success":           true,
			"captchaSiteKey":    grecaptchaSiteKey,
			"botname":           Mumble.Self.Name,
			"maxUploadTime":     uploadMaxDuration,
			"defaultUploadTime": uploadDefaultDuration,
		})
	})
	Handle("/api/points", func(r *Request) {
		user := r.GetUser()
		if user != nil {
			r.WriteJSON(200, JSON{
				"points":  user.Points,
				"success": true,
			})
		}
	})
	Handle("/api/updateUserPerms", func(r *Request) {
		user := r.GetUser()
		if user != nil {
			if !(user.Has(PermissionUserChangePerms) || (user.ID == 0 && !user.Service)) {
				r.Error(401, errors.New("Missing permission"))
				return
			}
			targets := r.R.FormValue("target")
			target, err := strconv.ParseUint(targets, 10, 32)
			if targets == "" || err != nil {
				r.Error(500, errors.New("Missing target"))
				return
			}
			perms := r.R.FormValue("perm")
			perm, err := strconv.ParseUint(perms, 10, 32)
			if perms == "" || err != nil {
				r.Error(500, errors.New("Missing perm"))
				return
			}
			t := Load(uint32(target), false)
			if t == nil {
				r.Error(500, errors.New("Cannot find user"))
				return
			}
			t.Permissions = Permission(perm)
			_, err = Db.C("UserData").Upsert(bson.M{
				"ID": t.ID,
			}, t.ToDisk())
			if err != nil {
				r.Error(500, err)
				return
			}
			r.WriteJSON(200, JSON{
				"success": true,
			})
			t.SendFullUpdate(false)
		}
	})
	Handle("/api/pointsPlus", func(r *Request) {
		user := r.GetUser()
		if user != nil {
			if user.Service {
				r.Error(400, errors.New("Invalid for service users"))
				return
			}
			success := ValidateCaptcha(r.R.FormValue("capTok"), "")
			if success {
				user.Points += webCaptchaPoints
			}
			r.WriteJSON(200, JSON{
				"success": success,
			})
			user.SendPoints()
		}
	})
	Handle("/api/moveAll", func(r *Request) {
		user := r.GetUser()
		if user != nil {
			err := MoveAllTo(user)
			if err != nil {
				r.Error(401, err)
				return
			}
			r.WriteJSON(200, JSON{
				"success": true,
			})
		}
	})
	Handle("/api/chat", func(r *Request) {
		user, err := GetUserByKey(r.R.URL.Query().Get("key"))
		if err != nil {
			r.Error(401, err)
		}
		if user != nil {
			bstr, err := ioutil.ReadAll(r.R.Body)
			if err != nil {
				r.Error(500, err)
				return
			}
			str := string(bstr)
			inline := ParseBool(r.R.URL.Query().Get("inline"), true)
			if !inline {
				if err != nil {
					r.WriteJSON(400, JSON{
						"success": false,
						"error":   err.Error(),
					})
					return
				}
			}
			if inline {
				var nstr string
				nstr, err = TryInlining(str)
				if nstr != "" {
					str = nstr
				}
			}
			Mumble.Channels[0].Send(user.ChatName()+str, true)
			status := 200
			res := JSON{
				"success": true,
			}
			if err != nil {
				res["error"] = err.Error()
			}
			r.WriteJSON(status, res)
		}
	})
}
