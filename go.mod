module bitbucket.org/abex/yumeko

go 1.12

require (
	bitbucket.org/abex/go-optional v0.0.0-20150902044611-5304370459de
	bitbucket.org/abex/pathfinder v0.0.0-20170507112819-bdce8b2efbc9 // indirect
	bitbucket.org/abex/sliceconv v0.0.0-20151017061159-594a23261816
	github.com/boombuler/barcode v1.0.0
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/gorilla/websocket v1.4.0
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a // indirect
	gitlab.com/yumeko/MumbleEmu v0.0.0-20170923112213-54c9892f02e9
	golang.org/x/net v0.0.0-20190501004415-9ce7a6920f09
	golang.org/x/sys v0.0.0-20190429190828-d89cdac9e872
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/fsnotify/fsnotify.v1 v1.4.7
	gopkg.in/ini.v1 v1.42.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
