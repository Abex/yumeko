// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package util

import (
	"fmt"
	"math"
	"strconv"
)

var sizeISOLut = []string{
	"B",
	"KiB",
	"MiB",
	"GiB",
	"TiB",
	"PiB",
	"EiB",
	"ZiB",
	"YiB",
}

func SizeToISO(val int64) string {
	i := int(math.Log(float64(val)/10) / math.Log(1024))
	if i > len(sizeISOLut) {
		i = len(sizeISOLut)
	}
	num := float64(val) / math.Pow(1024, float64(i))
	return fmt.Sprintf("%v %v", strconv.FormatFloat(num, 'f', 2, 64), sizeISOLut[i])
}
