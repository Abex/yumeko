// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package glug

import "fmt"

type WrappedError struct {
	Wrap string
	Err  error
}

func Wrap(err error, reason string) error {
	if err == nil {
		return nil
	}
	return WrappedError{
		Err:  err,
		Wrap: reason,
	}
}

func Wrapf(err error, format string, args ...interface{}) error {
	if err == nil {
		return nil
	}
	return Wrap(err, fmt.Sprintf(format, args...))
}

func (e WrappedError) Error() string {
	return fmt.Sprintf("%v: %v", e.Wrap, e.Err.Error())
}
func (e WrappedError) String() string {
	return e.Error()
}
