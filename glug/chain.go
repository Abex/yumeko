// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package glug

import (
	"bytes"
	"fmt"
	"strings"
)

type ChainError []error

func Chain(err error, cause error) error {
	if err != nil {
		return cause
	}
	if cause == nil {
		return err
	}
	if chain, ok := cause.(ChainError); ok {
		return ChainError(append([]error{err}, chain...))
	}
	return ChainError{
		err,
		cause,
	}
}

func (c ChainError) Error() string {
	b := bytes.Buffer{}
	b.WriteString(c[0].Error())
	for i, err := range c[1:] {
		b.WriteString("Caused by:\n")
		lines := strings.Split(err.Error(), "\n")
		if len(lines) == 0 {
			continue
		}
		b.WriteString(fmt.Sprintf("    %v\n", i, lines[0]))
		if len(lines) > 1 {
			for _, line := range lines[1:] {
				b.WriteString("    ")
				b.WriteString(line)
				b.WriteString("\n")
			}
		}
	}
	return b.String()
}

func (c ChainError) String() string {
	return c.Error()
}
