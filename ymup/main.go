// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"os/user"
	"path/filepath"

	"bitbucket.org/abex/go-optional"
	"bitbucket.org/abex/yumeko/versioning"
)

//ConfigT is the config file
type ConfigT struct {
	Version bool `arg:"v" arg:"version" json:"-"`

	Config string `arg:"c" arg:"config" json:"-"`
	Save   bool   `arg:"save" json:"-"`
	Help   bool   `arg:"help" json:"-"`

	Soundboard bool   `arg:"s" arg:"soundboard" arg:"!u" json:"-"`
	Host       string `arg:"h" arg:"host" json:",omitempty"`
	Key        string `arg:"k" arg:"key" json:",omitempty"`

	File     string `arg:"f" arg:"file" json:"-"`
	Name     string `arg:"n" arg:"name" json:"-"`
	Duration int    `arg:"d" arg:"duration" json:"-"`
	Inline   bool   `arg:"i" arg:"inline" arg:"!q" json:"-"`
}

var help = `Usage: ymup [options]

Uploads to yumeko's soundboard or file upload

Arguments
  -v, --version    Print version and exit
  -c, --config     Config file to use. Default is ~/.ymup
  -d, --duration   File upload only. Minutes to keep before purging
  -f, --file       File to upload. Required
      --help       Shows this text
  -h, --host       Specifies the host to connect to. Should include protocol
                     example: https://example.com
  -i, --inline     File upload only. Send in chat when finished
  -k, --key        Your login token. PM Yumeko 'login' to retrieve
  -n, --name       Name of the file to upload. Inferred by file name for file
                     uploads. Set to 'default' to take the server's suggustion
                     for soundboard uploads ONLY.
      --save       Save Key and Host to the config file
  -s, --soundboard Upload to the soundboard instead of the file upload

Examples
  ymup --save -k 1234567890 -h http://example.com
    Save the key and host to '~\.ymup' for use as defaults
  ymup -s -f music.mp3 -n default
    Upload music.mp3 to the soundboard with the suggested name

Config 
  The config file must be valid JSON with optionally the keys
  'Host' and 'Key'. No other keys are accepted
`

func must(quiet bool, val, name string) {
	if val == "" {
		if quiet {
			os.Exit(0)
		}
		fmt.Printf("%v is requred\n%v\n", name, help)
		os.Exit(1)
	}
}

func main() {
	config := ConfigT{}
	opts := optional.New(&config)
	opts.AddSource(2, opts.Arguments(os.Args))
	if config.Help {
		fmt.Println(help)
		return
	}
	cfgFileName := "~/.ymup"
	if config.Config != "" {
		cfgFileName = config.Config
	}
	if cfgFileName[0] == '~' {
		userr, err := user.Current()
		if err != nil {
			panic(err)
		}
		cfgFileName = filepath.Join(userr.HomeDir, cfgFileName[2:])
	}
	func() {
		cfgFi, err := os.Open(cfgFileName)
		if err != nil {
			if config.Config != "" {
				panic(err)
			}
			return
		}
		defer cfgFi.Close()
		cfgStr, err := ioutil.ReadAll(cfgFi)
		if err != nil {
			panic(err)
		}
		if len(cfgStr) < 2 && config.Config == "" && !config.Save {
			return
		}
		cfg := map[string]interface{}{}
		err = json.Unmarshal(cfgStr, &cfg)
		if err != nil {
			panic(err)
		}
		opts.AddSource(1, optional.Wrapped{cfg})
	}()
	if config.Version {
		fmt.Println(versioning.LongVersion())
		return
	}
	if config.Save {
		func() {
			cfgFi, err := os.OpenFile(cfgFileName, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0777)
			if err != nil {
				panic(err)
			}
			defer cfgFi.Close()
			cfgStr, err := json.MarshalIndent(config, "", "   ")
			if err != nil {
				panic(err)
			}
			_, err = cfgFi.Write(cfgStr)
			if err != nil {
				panic(err)
			}
		}()
	}
	must(config.Save, config.Host, "Host")
	must(config.Save, config.Key, "Key")
	must(config.Save, config.File, "File")
	if config.Soundboard && config.Duration != 0 {
		fmt.Println("Duration ignored")
	}
	if config.Soundboard && config.Inline {
		fmt.Println("Inline ignored")
	}
	fi, err := os.Open(config.File)
	if err != nil {
		panic(err)
	}
	defer fi.Close()
	if config.Soundboard {
		ret := struct {
			Error string `json:"error"`
			Name  string
			ID    string
		}{}
		opts := map[string]string{
			"key": config.Key,
		}
		err := uploadMultipart(config.Host+"/api/soundboard/upload", opts, "audio", filepath.Base(config.File), fi, &ret)
		if err != nil {
			panic(err)
		}
		if ret.Error != "" {
			fmt.Printf("Error: %v\n", ret.Error)
			return
		}
		name := ret.Name
		if config.Name == "" {
			fmt.Println("\nNo name given. Press enter to accept or type a new name:")
			fmt.Println(ret.Name)
			r := bufio.NewReader(os.Stdin)
			nname, err := r.ReadString('\n')
			if len(nname) >= 1 && nname[len(nname)-1] == '\n' {
				nname = nname[:len(nname)-1]
			}
			if len(nname) >= 1 && nname[len(nname)-1] == '\r' {
				nname = nname[:len(nname)-1]
			}
			if err != nil {
				panic(err)
			}
			if nname != "" {
				name = nname
			}
		} else if config.Name != "default" {
			name = config.Name
		}
		form := url.Values{}
		form.Set("key", config.Key)
		form.Set("id", ret.ID)
		form.Set("name", name)
		res, err := http.PostForm(config.Host+"/api/soundboard/uploadFinish", form)
		if err != nil {
			panic(err)
		}
		status := struct {
			Success bool   `json:"success"`
			Error   string `json:"error"`
		}{}
		cnt, err := ioutil.ReadAll(res.Body)
		if err != nil {
			panic(err)
		}
		err = json.Unmarshal(cnt, &status)
		if err != nil {
			panic(err)
		}
		if status.Success {
			fmt.Println("Success!")
		} else {
			fmt.Println("Error:", status.Error)
		}
		res.Body.Close()
	} else {
		name := config.Name
		if name == "" {
			name = filepath.Base(config.File)
		}
		status := struct {
			Success bool   `json:"success"`
			Path    string `json:"path"`
			Error   string `json:"error"`
		}{}
		opts := map[string]string{
			"key": config.Key,
		}
		if config.Inline {
			opts["inline"] = "1"
		}
		if config.Duration != 0 {
			opts["expire"] = fmt.Sprint(config.Duration)
		}
		err := uploadMultipart(config.Host+"/api/upload/upload", opts, "upload", name, fi, &status)
		if err != nil {
			panic(err)
		}
		if !status.Success {
			fmt.Printf("Error: %v\n", status.Error)
			return
		}
		fmt.Println("")
		fmt.Println(status.Path)
	}
}

func uploadMultipart(url string, params map[string]string, fikey, finame string, fi io.Reader, out interface{}) error {
	formStream, err := ioutil.TempFile("", ".ymup_")
	if err != nil {
		return err
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		_, ok := <-c
		if !ok {
			return
		}
		formStream.Close()
		os.Remove(formStream.Name())
		os.Exit(0)
	}()
	defer func() {
		close(c)
		formStream.Close()
		os.Remove(formStream.Name())
	}()
	form := multipart.NewWriter(formStream)
	for k, v := range params {
		err := form.WriteField(k, v)
		if err != nil {
			return err
		}
	}
	formFile, err := form.CreateFormFile(fikey, finame)
	if err != nil {
		return err
	}
	_, err = io.Copy(formFile, fi)
	if err != nil {
		return err
	}
	form.Close()
	lengt, err := formStream.Seek(0, 1)
	if err != nil {
		return err
	}
	_, err = formStream.Seek(0, 0)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", url, &ProgressReader{
		R:   formStream,
		Len: lengt,
	})
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", form.FormDataContentType())
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	//fmt.Println(res.Status)
	//fmt.Println(string(cnt))
	cnt, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(cnt, out)
	res.Body.Close()
	return err
}

//ProgressReader prints progress when reading
type ProgressReader struct {
	R   io.Reader
	Len int64
	Cur int
}

const full = "##################################################"
const empty = "                                                  "

var sizes = [...]string{"B", "KiB", "MiB", "GiB", "TiB", "PiB"}

func sizefmt(v float64) string {
	post := ""
	for _, ipost := range sizes {
		post = ipost
		if v <= 1024 {
			break
		}
		v /= 1024
	}
	return fmt.Sprintf("%0.1f%s", v, post)
}

func (p *ProgressReader) Read(d []byte) (n int, err error) {
	n, err = p.R.Read(d)
	p.Cur += n
	cur := float64(p.Cur)
	leng := float64(p.Len)
	v := int((cur / leng) * 50)
	fmt.Fprintf(os.Stderr, "[%s%s] %v / %v \r", full[:v], empty[v:], sizefmt(cur), sizefmt(leng))
	return
}
