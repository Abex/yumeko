var Log={};
(function(){
  var C=function(name){
    Log[name]=function(e){
      console.log(name+":",e);
    };
  };
  var A=function(name){
    Log[name]=function(e){
      if(e){
        alert(name+":",e);
      }else{
        alert(name);
      }
    };
  };
  var E=function(name){
    Log[name]=function(e){
      var v = e;
      if(e.data&&e.data.length>100){
        v=e.data.substr(0,100)+"...";
      }
      console.log(name+":",v)
    }
  }
  C("HTTPError");
  E("WSOut");
  C("WSError");
  E("WSEvent");
  C("WSOpen");
  C("WSClose");
  C("TabError");
  A("PermMissing")
}());
