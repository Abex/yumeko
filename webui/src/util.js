"use strict";
if(navigator.userAgent.match(/(iPad|iPhone|iPod)/g)){
  window.onerror = function(msg, file, line, col, error) {
    //alert(msg);
    //alert(file+line+col);
  };
}else{
  window.onerror = function(msg, file, line, col, error) {
    console.log(error);
  };
  pl(document.documentElement).addClass("usePerm");
}


//Layout 'container' 'row' 'item'
xtag.register('l-c',{});
xtag.register('l-r',{});
xtag.register('l-i',{});

function MakeProp(obj,name){
  if(obj["_b"+name]) return;
  var t = obj[name];
  if (typeof t!="object" || !t.hasOwnProperty("v")){
    t={
      v:t,
      setters:[],
    };
  }
  Object.defineProperty(obj,"_b"+name,{
    value:t,
    writable:true,
    configurable:true,
    enumerable:false,
  });
  delete obj[name];
  Object.defineProperty(obj,name,{
    get:function(){
      return t.v;
    },
    set:function(v){
      if (t.setters && t.setters.length){
        for(var i=0;i<t.setters.length;i++){
          if(t.setters[i]){
            t.v=v;
            v=t.setters[i](v);
          }
        }
      }
      t.v=v;
    },
    enumerable:true,
    configurable:true,
  });
  obj[name]=obj[name];
}

function ForEach(array,each,done){
  var i=0;
  var f=function(){
    for(var tend=performance.now()+8;performance.now()<tend;){
      var e=Math.min(i+20,array.length);
      for(;i<e;i++){
        each(array[i],i);
      }
      if(i>=array.length) break;
    }
    if(i>=array.length){
      requestAnimationFrame(done);
    }else{
      requestAnimationFrame(f);
    }
  };
  f();
}

function GetObjByName(v){
  var h=window;
  var ii="";
  var as=false;
  var i=0;
  v="."+v;
  var cu=function(a,b){
    var acc="";
    for(;i<v.length;i++){
            if(a===v[i]||b===v[i]){
              return acc;
            }
      if(v[i]==="\\"){
        i++//we dont worry about \n because we probably wont have to deal with that
         // TODO: deal with \n properly (and \x0000 etc)
      }
      acc+=v[i];
    }
    return acc;
  };
  for(;i<v.length;){
    var c=v[i];
    var tok="";
    if(c==="["){
      c=v[++i];
      var lf="]";
      if(c==="\""||c==="'"){
        lf=c
        ++i;
      }
      tok=cu(lf);
      if(lf!=="]"){
        ++i;
      }else{
        tok=~~tok;
      }
      ++i;
    }else{//c==="." in theory
      ++i;
      tok=cu(".","[");
    }
    if(as){
      h=h[ii];
    }
    ii=tok;
    as=true;
  }
  return [h,ii];
}

function AddSetter(obj,name,setter){
  try{
    if(!obj["_b"+name]){
      MakeProp(obj,name);
    }
    if(!obj["_b"+name].setters) obj["_b"+name].setters=[];
    var id=obj["_b"+name].setters.length;
    obj["_b"+name].setters[id]=setter;
    obj[name]=obj[name];
    return id;
  }catch(e){
    console.log(e);
    throw {
      Obj:obj,
      Name:name,
    };
  }
}

function RemoveSetter(obj,name,id){
  obj["_b"+name].setters[id]=null;
}

function xtagChildBinder(name){
  return {
    attribute:{},
    get:function(){
      return this.children[0][name];
    },
    set:function(v){
      if(this.children[0][name]!=v)this.children[0][name]=v;
    },
  };
}

var StrCmp = function(){
  if(window["Intl"]&&Intl.Collator){
    var coll = new Intl.Collator("en-us-u-co-phonebk-kn-true",{sensitivity:"accent"});
    return coll.compare.bind(coll);
  }else{
    return function(a,b){
      return a-b;
    }
  }
}();

xtag.register('x-inp',{
  content:'<input/>',
  lifecycle:{
    created:function(){
      var that=this;
      this._eventListener = function(e){
        that.value=e.target[that.type=="checkbox"?"checked":"value"];
      };
      this.children[0].addEventListener("input",this._eventListener);
      this.children[0].addEventListener("change",this._eventListener);
    },
    removed:function(){
      if(this._bindData){
        RemoveSetter(this._bindData.t[0],this._bindData.t[1],this._bindData.id);
      }
      this.children[0].removeEventListener("input",this._eventListener);
      this.children[0].removeEventListener("change",this._eventListener);
    },
  },
  accessors: {
    type:xtagChildBinder("type"),
    min:xtagChildBinder("min"),
    max:xtagChildBinder("max"),
    step:xtagChildBinder("step"),
    placeholder:xtagChildBinder("placeholder"),
    disabled:xtagChildBinder("disabled"),
    value:{
      attribute:{},
      get:function(){
        return this.children[0][this.type=="checkbox"?"checked":"value"];
      },
      set:function(v){
        if(this.children[0][this.type=="checkbox"?"checked":"value"]!=v) this.children[0][this.type=="checkbox"?"checked":"value"]=v;
        if(this._bindData && this._bindData.t[0][this._bindData.t[1]]!=v){
          this._bindData.t[0][this._bindData.t[1]]=v;
        }
      },
    },
    bind:{
      attribute:{},
      get:function(){
        return this._bindData.text||"";
      },
      set:function(v){
        if(this._bindData){
          RemoveSetter(this._bindData.t[0],this._bindData.t[1],this._bindData.id);
        }
        var that=this;
        var t = GetObjByName(v);
        var id=AddSetter(t[0],t[1],function(v){
          if(that.value!=v)that.value=v;
          return v;
        });
        this._bindData={
          text:v,
          t:t,
          id:id,
        };
      }
    },
  },
});

xtag.register("x-out",{
  lifecycle:{
    created:function(){
      this.appendChild(document.createTextNode(""));
      this._fmtFunc=function(v){return v;};
    },
    removed:function(){
      if(this._bindData){
        RemoveSetter(this._bindData.t[0],this._bindData.t[1],this._bindData.id);
      }
    },
  },
  accessors: {
    bind:{
      attribute:{},
      get:function(){
        return this._bindData.text||"";
      },
      set:function(v){
        if(this._bindData){
          RemoveSetter(this._bindData.t[0],this._bindData.t[1],this._bindData.id);
        }
        var that=this;
        var t = GetObjByName(v)
        var id=AddSetter(t[0],t[1],function(v){
          var o = that._fmtFunc(v)
          if(o===null||o===undefined) o="\xa0";
          that.replaceChild(document.createTextNode(o),that.childNodes[0]);
          return v;
        });
        this._bindData={
          text:v,
          t:t,
          id:id,
        };
      }
    },
    fmt:{
      attribute:{},
      set:function(v){
        this._fmt=v;
        var t = GetObjByName(this._fmt);
        this._fmtFunc=t[0][t[1]];
        if(this._bindData){
          this._bindData.t[0][this._bindData.t[1]]=this._bindData.t[0][this._bindData.t[1]];
        }
      },
      get:function(){
        return this._fmt||"";
      }
    }
  },
});

xtag.register("x-prog",{
  lifecycle:{
    created:function(){
      this.appendChild(document.createElement("span"));
    },
    removed:function(){
      if(this._bindData){
        RemoveSetter(window[this._bindData.t[0]],this._bindData.t[1],this._bindData.id);
      }
    },
  },
  accessors: {
    bind:{
      attribute:{},
      get:function(){
        return this._bindData.text||"";
      },
      set:function(v){
        if(this._bindData){
          RemoveSetter(window[this._bindData.t[0]],this._bindData.t[1],this._bindData.id);
        }
        var that=this;
        var t = GetObjByName(v);
        var id=AddSetter(t[0],t[1],function(v){
          that.childNodes[0].style.width=(v*100)+"%";
          return v;
        });
        this._bindData={
          text:v,
          t:t,
          id:id,
        };
      }
    },
  },
});

xtag.register("x-click",{
  lifecycle:{
    created:function(){
      var that=this;
      this._d={};
      this._eventListener=function(e){
        if(that.disabled)return;
        var fn = that.bind;
        ["ctrl","shift","alt"].some(function(k){
          if(e[k+"Key"]&&that[k]){
            fn = that[k];
            return 1;
          }
        });
        eval(fn||"");
      };
      this.addEventListener("click",this._eventListener);
    },
    removed:function(){
      this.removeEventListener("click",this._eventListener);
    },
  },
  accessors:{
    bind:{attribute:{}},
    alt:{attribute:{}},
    shift:{attribute:{}},
    ctrl:{attribute:{}},
    disabled:{attribute:{}},
  }
});

function BindTo(obj,name,target,tname){
  return AddSetter(obj,name,function(v){
    target[tname]=v;
    return v;
  });
}

function SortInsert(parent,newNode,comp,start){
  var min=start||0;
  var len = parent.children.length;
  var max=len;
  var mid=0;
  while(min<max){
    mid=((min+max)/2)|0;
    if(mid==len){
      mid=len-1;
      break;
    }
    var v = comp(parent.children[mid],newNode);
    if(v<0){
      min=mid+1;
    }else{
      max=mid;
    }
  }
  if(mid && comp(parent.children[mid],newNode)==1)mid--;//Shift if needed
  var el;
  if(parent.children[mid]){
    el=parent.children[mid].nextSibling;
  }else{
    el=null;
  }
  parent.insertBefore(newNode,el);
}
