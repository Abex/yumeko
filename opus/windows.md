# Opus on Windows
- Get pkg-config
  - `go get github.com/rjeczalik/pkgconfig && go install github.com/rjeczalik/pkgconfig`
  - Make sure that `%GOPATH%\bin` is in your path

- Get the libopus [headers](https://www.opus-codec.org/downloads/)
  - Put them in `%GOPATH%\include\opus\`

- Get the libopus lib
  - I get them from [cygwin](https://www.cygwin.com/), but you can compile them yourself, with GCC.
  - Put opus.lib and opus.pc(From this folder) in `%GOPATH%\lib\windows_amd64\opus\`
  - Put `cygopus-0.dll` and `cygwin1.dll` in `%GOPATH%\bin\`
