// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package opus

/*#include "opus.h"
void gopDReset(OpusDecoder* e){
	opus_decoder_ctl(e,OPUS_RESET_STATE);
}
opus_uint32 gopDFinalRange(OpusDecoder* e){
	opus_uint32 a;
	opus_decoder_ctl(e,OPUS_GET_FINAL_RANGE(&a));
	return a;
}
opus_int32 gopDBandwidth(OpusDecoder* e){
	opus_int32 a;
	opus_decoder_ctl(e,OPUS_GET_BANDWIDTH(&a));
	return a;
}
opus_int32 gopDPitch(OpusDecoder* e){
	opus_int32 a;
	opus_decoder_ctl(e,OPUS_GET_PITCH(&a));
	return a;
}
void gopDSetGain(OpusDecoder* e,opus_int32 v){
	opus_decoder_ctl(e,OPUS_SET_GAIN(v));
}
opus_int32 gopDGain(OpusDecoder* e){
	opus_int32 a;
	opus_decoder_ctl(e,OPUS_GET_GAIN(&a));
	return a;
}*/
import "C"
import (
	"errors"
	"unsafe"
)

//Decoder is a opus decoder
type Decoder C.struct_OpusDecoder

//GetDecoderSize returns the size of a Decoder
func GetDecoderSize(channels int) int {
	return int(C.opus_decoder_get_size(C.int(channels)))
}

//NewDecoder Allocates and initializes an decoder state.
func NewDecoder(sampleRate int, channels int) (*Decoder, error) {
	enc := (*Decoder)((unsafe.Pointer)(&make([]byte, GetDecoderSize(channels))[0]))
	e := C.opus_decoder_init((*C.struct_OpusDecoder)(enc), C.opus_int32(sampleRate), C.int(channels))
	return enc, getError(e)
}

//Decodes a opus frame from pcm input
func (e *Decoder) DecodeInt16(data []byte, frameSize int, fec bool) ([]int16, error) {
	pcm := make([]int16, frameSize)
	ifec := 0
	if fec {
		ifec = 1
	}
	var dataptr *byte
	if len(data) > 0 {
		dataptr = &data[0]
	}
	ret := C.opus_decode((*C.struct_OpusDecoder)(e), (*C.uchar)(dataptr), C.opus_int32(len(data)), (*C.opus_int16)(&pcm[0]), C.int(frameSize), C.int(ifec))
	var err error
	if ret < 0 {
		err = getError(C.int(ret))
		pcm = nil
	} else {
		pcm = pcm[:ret]
	}
	return pcm, err
}

//Decodes an Opus frame from floating point input.
func (e *Decoder) DecodeFloat32(data []byte, frameSize int, fec bool) ([]float32, error) {
	pcm := make([]float32, frameSize)
	ifec := 0
	if fec {
		ifec = 1
	}
	var dataptr *byte
	if len(data) > 0 {
		dataptr = &data[0]
	}
	ret := C.opus_decode_float((*C.struct_OpusDecoder)(e), (*C.uchar)(dataptr), C.opus_int32(len(data)), (*C.float)(&pcm[0]), C.int(frameSize), C.int(ifec))
	var err error
	if ret < 0 {
		err = getError(C.int(ret))
		pcm = nil
	} else {
		pcm = pcm[:ret]
	}
	return pcm, err
}

//Gets the number of samples of an Opus packet.
func (e *Decoder) GetNbSamples(data []byte) (int, error) {
	ret := C.opus_decoder_get_nb_samples((*C.struct_OpusDecoder)(e), (*C.uchar)(&data[0]), C.opus_int32(len(data)))
	if ret == C.OPUS_BAD_ARG {
		return 0, errors.New("OPUS_BAD_ARG: Insufficient data was passed to the function.")
	}
	if ret == C.OPUS_INVALID_PACKET {
		return 0, errors.New("OPUS_INVALID_PACKET: The compressed data passed is corrupted or of an unsupported type.")
	}
	return int(ret), nil
}

//Resets the codec state to be equivalent to a freshly initialized state.
//This should be called when switching streams in order to prevent the back to back decoding from giving different results from one at a time decoding.
func (e *Decoder) Reset() {
	C.gopDReset((*C.struct_OpusDecoder)(e))
}

//Gets the final state of the codec's entropy coder.
func (e *Decoder) GetFinalRange() uint32 {
	return uint32(C.gopDFinalRange((*C.struct_OpusDecoder)(e)))
}

//Gets the decoder's configured bandpass or the decoder's last bandpass.
func (e *Decoder) Bandwidth() Bandwidth {
	return Bandwidth(C.gopDBandwidth((*C.struct_OpusDecoder)(e)))
}

//Gets the pitch of the last decoded frame, if available.
//This can be used for any post-processing algorithm requiring the use of pitch, e.g. time stretching/shortening. If the last frame was not voiced, or if the pitch was not coded in the frame, then zero is returned.
//This CTL is only implemented for decoder instances.
func (e *Decoder) Pitch() int {
	return int(C.gopDBandwidth((*C.struct_OpusDecoder)(e)))
}

//Gets the decoder's configured gain adjustment.
func (e *Decoder) Gain() int {
	return int(C.gopDGain((*C.struct_OpusDecoder)(e)))
}

//Configures decoder gain adjustment.
//Scales the decoded output by a factor specified in Q8 dB units. This has a maximum range of -32768 to 32767 inclusive, and returns OPUS_BAD_ARG otherwise. The default is zero indicating no adjustment. This setting survives decoder reset.
//gain = pow(10, x/(20.0*256))
func (e *Decoder) SetGain(v int) {
	C.gopDSetGain((*C.struct_OpusDecoder)(e), C.opus_int32(v))
}
