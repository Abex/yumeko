package versioning

import (
	"fmt"
	"net/http"
	"path"
	"runtime/debug"
)

func ShortVersion() string {
	info, ok := debug.ReadBuildInfo()
	if !ok {
		panic("Needs modules")
	}
	name := path.Base(info.Main.Path)
	return fmt.Sprintf("%v %v", name, info.Main.Version)
}

func LongVersion() string {
	info, ok := debug.ReadBuildInfo()
	if !ok {
		panic("Needs modules")
	}
	name := path.Base(info.Main.Path)
	return fmt.Sprintf("%v %v (%v)", name, info.Main.Version, info.Main.Sum)
}

func UserAgent() string {
	info, ok := debug.ReadBuildInfo()
	if !ok {
		panic("Needs modules")
	}
	name := path.Base(info.Main.Path)
	return fmt.Sprintf("%v/%v (+https://bitbucket.org/abex/yumeko)", name, info.Main.Version)
}

func SetUserAgent(req *http.Request) {
	req.Header.Set("User-Agent", UserAgent())
}
