var Theme ={
  Current:{
    v:localStorage["theme"]||"None",
    setters:[
      function(v){
        localStorage["theme"]=v;
        return v;
      },
    ],
  },
};
pl(function(){
  var ts = document.getElementById("themeSel");
  AddSetter(Theme,"Current",function(v){
    document.getElementById("userStyle").href="themes/"+v+".css";
    if(ts.value!=v)ts.value=v;
    return v;
  });
  ts.onchange=function(){
    Theme.Current = ts.value;
  };
});
