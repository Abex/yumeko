// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/abex/yumeko/audioInfo"
	"bitbucket.org/abex/yumeko/conf"

	"gopkg.in/mgo.v2/bson"
)

var sbClipDir string
var sbMaxPrice uint
var sbMaxOverlayTime int
var sbIcecastStreamURI string

func init() {
	conf.Config(&sbClipDir, "soundboard", `
# Directory to put soundboard clip data in
clip-dir = "clips/"
`)
	conf.Config(&sbMaxPrice, "soundboard", `
# The maximum cost of a single clip in points
max-price = 25
`)
	conf.Config(&sbMaxOverlayTime, "soundboard", `
# How long can a clip play in the overlay channel before being killed.
# In seconds
max-overlay-time = 4
`)
	conf.Config(&sbIcecastStreamURI, "soundboard", `
# What stream to open when '/api/soundboard/icecast' is POSTed to by
# a service user
icecast-stream-uri = ""
`)
}

//Exists returns true if a file exists
func Exists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

//A Clip represents the format of a soundboard clip
type Clip struct {
	Name            string    `bson:"Name"`
	ID              string    `bson:"ID"`
	Duration        uint32    `bson:"Duration"`
	UploadDate      time.Time `bson:"UploadDate"`
	Volume          float32   `bson:"Volume"`
	CostOverride    int32     `bson:"CostOverride"`
	ChannelMode     uint8     `bson:"ChannelMode"`
	UseMusicEncoder uint8     `bson:"UseMusicEncoder"`
	Extra           bson.M    `bson:",inline"`
}

//ClientClip is the format of a clip send to the client
type ClientClip struct {
	Name            string
	ID              string
	Duration        uint32
	UploadDate      time.Time
	Volume          float32
	Cost            uint
	ChannelMode     uint8
	UseMusicEncoder uint8
}

//Client returns the client representation of a Clip
func (c Clip) Client() (r ClientClip) {
	r.Name = c.Name
	r.ID = c.ID
	r.Duration = c.Duration
	r.UploadDate = c.UploadDate
	r.Volume = c.Vol()
	r.Cost = c.Cost()
	r.ChannelMode = c.ChannelMode
	r.UseMusicEncoder = c.UseMusicEncoder
	return
}

//Path returns the actual path of the clip
func (c Clip) Path() string {
	return path.Join(conf.Path(sbClipDir), c.ID)
}

//Cost returns the cost from the override or calculates it
func (c Clip) Cost() uint {
	if c.CostOverride > 0 {
		return (uint)(c.CostOverride)
	}
	price := (uint)(math.Ceil(((float64)(c.Duration) / 10.0) * (float64(c.Vol()) / 2.0)))
	if price > sbMaxPrice {
		return sbMaxPrice
	}
	if price < 1 {
		return 1
	}
	return price
}

//Vol returns the volume of the track
func (c Clip) Vol() (vol float32) {
	vol = c.Volume
	if vol <= 0 {
		vol = 1
	}
	return
}

//SbPlay id a sound, deducts from the user
func SbPlay(id string, user *User, interruptWager uint, overlay bool) error {
	if !user.Service {
		mUser := user.M()
		if mUser == nil {
			return errors.New("Not Connected")
		}
	}

	if !user.Has(PermissionSBPlay) {
		return errors.New("Missing permission")
	}

	var clip Clip

	err := Db.C("Soundboard").Find(bson.M{
		"ID": id,
	}).One(&clip)

	if err != nil {
		return err
	}

	if interruptWager > 0 {
		err = user.Deduct(interruptWager + clip.Cost())
	} else {
		err = user.Deduct(clip.Cost())
	}

	if err != nil {
		return err
	}

	snd := &Sound{
		Name:        clip.Name,
		Owner:       user.Name,
		ID:          id,
		Duration:    clip.Duration,
		File:        clip.Path(),
		Inturrupt:   interruptWager,
		Channel:     SoundChannelMain,
		ChannelMode: clip.ChannelMode,
		EncoderMode: clip.UseMusicEncoder,
		Vol:         clip.Vol(),
	}
	if overlay {
		snd.Channel = SoundChannelOverlay
		snd.TimeLimit = int(sbMaxOverlayTime * 48000)
	}
	return snd.Play()
}

//ListTracks lists all the tracks in the Database
func ListTracks() (tracks []Clip, err error) {
	err = Db.C("Soundboard").Find(bson.M{}).Iter().All(&tracks)
	return
}

//CleanupClip cleans the clip of unused database entries; extended: run replaygain, try to autoset music mode
func CleanupClip(id string, extended, quiet bool) error {
	C := Db.C("Soundboard")
	var clip Clip
	err := C.Find(bson.M{
		"ID": id,
	}).One(&clip)
	if err != nil {
		return err
	}
	//Clear the extra
	clip.Extra = bson.M{}
	if extended {
		enc := (clip.UseMusicEncoder != 0) || (clip.Duration > 90)
		if enc {
			clip.UseMusicEncoder = 1
		} else {
			clip.UseMusicEncoder = 0
		}
		dbChange, err := audioInfo.GetReplayGain(clip.Path())
		if err == nil {
			clip.Volume = float32(math.Pow(10, -dbChange/20)) * .25
			if enc {
				clip.Volume *= .5
			}
		} else {
			fmt.Println(err)
		}
		if !quiet {
			body, err := json.Marshal(JSON{
				"ID":              clip.ID,
				"Volume":          clip.Volume,
				"UseMusicEncoder": clip.UseMusicEncoder,
			})
			if err == nil {
				WSGlobalDispatch(&WSResponse{
					URI:    "/api/soundboard/update",
					Status: 200,
					Body:   string(body),
				})
			}
		}
	}
	//Overwrite update
	C.Update(bson.M{
		"ID": clip.ID,
	}, clip)
	return nil
}

//StartSoundboard starts the Soundboard
func StartSoundboard() {
	os.Mkdir(conf.Path(sbClipDir), 0777)
	Handle("/api/soundboard/upload", func(w *Request) {
		if w.IsWS() {
			return
		}
		if w.Limit(HTTPLimiter) {
			return
		}
		if err := w.R.ParseMultipartForm(0); err != nil {
			w.Error(500, err)
			return
		}
		defer w.R.MultipartForm.RemoveAll()
		user := w.GetUser()
		if user != nil {
			if !user.Has(PermissionSBUpload) {
				w.Error(401, errors.New("Missing permission"))
				return
			}
			file, header, err := w.R.FormFile("audio")
			if err != nil {
				w.Error(500, err)
				return
			}
			defer file.Close()
			fitmp, err := ioutil.TempFile(os.TempDir(), "soundboard_upload")
			if err != nil {
				w.Error(500, err)
				return
			}
			defer func() {
				fitmp.Close()
				go func() {
					time.Sleep(time.Minute * 10)
					if Exists(fitmp.Name()) {
						os.Remove(fitmp.Name())
					}
				}()
			}()
			io.Copy(fitmp, file)
			name := header.Filename[0 : len(header.Filename)-len(path.Ext(header.Filename))]

			info, err := audioInfo.GetInfo(fitmp.Name())
			if err == nil && info.Title != "" {
				name = info.Title
				if info.Artist != "" {
					name = info.Title + " - " + info.Artist
				}
			}
			w.WriteJSON(201, JSON{
				"ID":   filepath.Base(fitmp.Name()),
				"Name": name,
			})
		}
	})
	Handle("/api/soundboard/uploadFinish", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			file := os.TempDir() + "/" + SanetizeFile(w.R.Form.Get("id"))
			if !Exists(file) {
				w.Error(500, errors.New("Id does not exist"))
				return
			}
			defer os.Remove(file)

			name := w.R.Form.Get("name")
			if name == "" {
				w.Error(500, errors.New("No Name submitted"))
				return
			}

			info, err := audioInfo.GetInfo(file)
			if err != nil {
				w.Error(500, err)
				return
			}

			permFile, err := ioutil.TempFile(conf.Path(sbClipDir), "")
			if err != nil {
				w.Error(500, err)
				return
			}
			defer permFile.Close()

			oldFile, err := os.Open(file)
			if err != nil {
				w.Error(500, err)
				return
			}
			defer oldFile.Close()

			io.Copy(permFile, oldFile)

			clip := Clip{
				Name:       name,
				ID:         filepath.Base(permFile.Name()),
				Duration:   uint32(info.Duration),
				UploadDate: time.Now(),
			}

			err = Db.C("Soundboard").Insert(clip)
			if err != nil {
				w.Error(500, err)
				return
			}
			CleanupClip(clip.ID, true, false)
			body, err := json.Marshal(clip.Client())
			if err == nil {
				WSGlobalDispatch(&WSResponse{
					URI:    "/api/soundboard/cbUpload",
					Status: 200,
					Body:   string(body),
				})
			}
			w.WriteJSON(200, JSON{
				"success": true,
			})
		}
	})
	Handle("/api/soundboard/list", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			tracks, err := ListTracks()
			if err != nil {
				w.Error(500, err)
				return
			}
			rt := make([]ClientClip, len(tracks))
			for k, v := range tracks {
				rt[k] = v.Client()
			}
			w.WriteJSON(200, rt)
		}
	})
	Handle("/api/soundboard/play", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			wager, _ := strconv.ParseUint(w.R.Form.Get("wager"), 10, 0)
			err := SbPlay(w.R.Form.Get("id"), user, uint(wager), ParseBool(w.R.Form.Get("overlay"), false))
			if err != nil {
				w.Error(500, err)
				return
			}
			w.WriteJSON(200, JSON{
				"success": true,
				"eapi":    true,
			})
		}
	})
	Handle("/api/soundboard/download", func(w *Request) {
		if w.IsWS() {
			return
		}
		if w.Limit(ServeRateLimit) {
			return
		}
		user := w.GetUser()
		if user != nil {
			var clip Clip
			err := Db.C("Soundboard").Find(bson.M{
				"ID": w.R.Form.Get("id"),
			}).One(&clip)

			if err != nil {
				w.Error(500, err)
				return
			}
			file, err := os.Open(clip.Path())
			if err != nil {
				w.Error(500, err)
				return
			}
			defer file.Close()
			name := strings.Replace(strings.Replace(clip.Name, "/", "_", -1), "\\", "", -1)
			ext, err := audioInfo.GetExtension(clip.Path())
			if err == nil && ext != "" {
				name += "." + ext
			}
			w.W.Header().Set("Content-Disposition", "attachment; filename=\""+name+"\"")
			http.ServeContent(w.W, w.R, "", clip.UploadDate, file)
		}
	})
	Handle("/api/soundboard/delete", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			if !user.Has(PermissionSBDelete) {
				w.Error(401, errors.New("Missing permission"))
				return
			}

			id := w.R.Form.Get("id")
			if id == "" {
				w.Error(400, errors.New("Bad ID"))
				return
			}

			var clip Clip
			err := Db.C("Soundboard").Find(bson.M{
				"ID": id,
			}).One(&clip)
			if err != nil {
				w.Error(500, err)
				return
			}
			err = Db.C("Soundboard").Remove(bson.M{
				"ID": id,
			})
			if err != nil {
				w.Error(500, err)
				return
			}
			err = os.Remove(clip.Path())
			if err != nil {
				fmt.Println(err)
			}

			w.DispatchJSON(200, "/api/soundboard/delete", JSON{
				"ID": id,
			})
		}
	})
	Handle("/api/soundboard/update", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			id := w.R.Form.Get("id")
			if id == "" {
				return
			}

			var clip Clip
			err := Db.C("Soundboard").Find(bson.M{
				"ID": id,
			}).One(&clip)
			if err != nil {
				w.Error(500, err)
				return
			}

			set := bson.M{}

			name := w.R.Form.Get("name")
			if name != "" && user.Has(PermissionSBUpdateName) {
				set["Name"] = name
			}

			volume := w.R.Form.Get("volume")
			if volume != "" && user.Has(PermissionSBUpdateVolume) {
				vol, err := strconv.ParseFloat(volume, 10)
				if vol < 0 {
					vol = 0
				}
				if vol > 6 {
					vol = 6
				}
				if err != nil {
					w.Error(500, err)
					return
				}
				set["Volume"] = vol
			}

			channelMode := w.R.Form.Get("channelmode")
			if channelMode != "" && user.Has(PermissionSBUpdateVolume) {
				chanMode, err := strconv.ParseInt(channelMode, 10, 8)
				if err != nil {
					w.Error(500, err)
					return
				}
				set["ChannelMode"] = chanMode
			}

			encoder := w.R.Form.Get("encoder")
			if encoder != "" && user.Has(PermissionSBUpdateVolume) {
				enc, err := strconv.ParseInt(encoder, 10, 8)
				if err != nil {
					w.Error(500, err)
					return
				}
				set["UseMusicEncoder"] = enc
			}

			cost := w.R.Form.Get("cost")
			if cost != "" && user.Has(PermissionSBUpdateCost) {
				costi, err := strconv.ParseUint(cost, 10, 0)
				if err != nil {
					w.Error(500, err)
					return
				}

				if uint(costi) != clip.Cost() {
					set["CostOverride"] = int32(costi)
					clip.CostOverride = int32(costi)
				}
			}

			err = Db.C("Soundboard").Update(bson.M{
				"ID": id,
			}, bson.M{
				"$set": set,
			})

			if err != nil {
				w.Error(500, err)
				return
			}
			set["ID"] = id
			set["cost"] = clip.Cost()
			w.DispatchJSON(200, "/api/soundboard/update", set)
		}
	})
	Handle("/api/soundboard/stop", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			err := user.Deduct(1)
			if err != nil {
				w.Error(401, err)
				return
			}
			var channel int
			{
				text := w.R.Form.Get("channel")
				channel64, _ := strconv.ParseInt(text, 10, 8)
				channel = int(channel64)
			}
			stopped := SoundStop(channel, false)
			w.WriteJSON(200, JSON{
				"success": stopped,
				"Channel": channel,
			})
		}
	})
	Handle("/api/soundboard/cleanup", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			if !user.Has(PermissionSBCleanup) {
				w.Error(401, errors.New("Missing Permission"))
				return
			}
			extended := ParseBool(w.R.Form.Get("extended"), false)
			id := w.R.Form.Get("id")
			if id == "all" {
				w.W.WriteHeader(202)
				it := Db.C("Soundboard").Find(bson.M{}).Iter()
				var clip Clip
				for it.Next(&clip) {
					err := CleanupClip(clip.ID, extended, false)
					if err == nil {
						err = errors.New("OK")
					} else {
						fmt.Printf("Cleanup: %v: %v\n", clip.ID, err)
					}
					fmt.Fprintf(w.W, "%v: %v\n", clip.ID, err)
				}
				w.Write("Finished!\n")
			} else {
				err := CleanupClip(id, extended, false)
				if err != nil {
					w.Error(500, err)
					return
				}
				w.WriteJSON(200, JSON{
					"success": true,
				})
			}
		}
	})
	Handle("/api/soundboard/volume", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			if !user.Has(PermissionSBChangePlayingVolume) {
				w.Error(401, errors.New("Missing Permission"))
				return
			}
			var chann int
			{
				channel := w.R.Form.Get("channel")
				chann64, _ := strconv.ParseInt(channel, 10, 8)
				chann = (int)(chann64)
			}
			if !ChannelIsPlaying(chann) {
				w.Error(401, errors.New("Nothing is playing"))
				return
			}
			var vol float32
			{
				volume := w.R.Form.Get("volume")
				vol64, err := strconv.ParseFloat(volume, 10)
				if err != nil {
					w.Error(500, err)
					return
				}
				if vol64 < 0 {
					vol64 = 0
				}
				if vol64 > 6 {
					vol64 = 6
				}
				vol = float32(vol64)
			}
			diff := Amplitude(chann) - vol
			if diff < 0 {
				diff *= diff
			}
			err := user.Deduct(uint(diff))
			if err != nil {
				w.Error(401, err)
				return
			}
			SetAmplitude(chann, vol)
			w.DispatchJSON(200, "/api/soundboard/volume", JSON{
				"Volume":  vol,
				"Channel": chann,
			})
		}
	})
	Handle("/api/soundboard/icecast", func(w *Request) {
		user := w.GetUser()
		if user == nil {
			if !user.Service {
				w.Error(403, errors.New("Not a service user"))
				return
			}
			snd := &Sound{
				Name:        user.Name,
				Owner:       "",
				File:        sbIcecastStreamURI,
				Inturrupt:   0,
				Channel:     SoundChannelBackground,
				ChannelMode: 0,
				EncoderMode: 1,
				Vol:         .5,
			}
			snd.Play()
			w.WriteJSON(200, JSON{
				"success": true,
			})
		}
	})
}
